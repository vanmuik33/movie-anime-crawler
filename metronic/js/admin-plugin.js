function table_detele(table, id) {

    Swal.fire({
        title: 'Bạn Có Chắc Chắn Xóa Trường Này',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Tôi Chắc Chắn!',
        cancelButtonText: 'Để Sau'
    }).then((result) => {
        if (result.isConfirmed) {
            $.post('/dashboard/graph-api', {
                table: table,
                id: id,
                action: 'table_detele'
            }, function(data) {
                let status = data.status,
                    message = data.message;
                if (status == 'false') {
                    Swal.fire(
                        'Deleted!',
                        message,
                        'danger'
                    );
                    return
                }
                Swal.fire(
                    'Deleted!',
                    message,
                    'success'
                )
                $(`#${table}_${id}`).remove();
            });
        }
    })
}

function cleaner_table(table) {

    Swal.fire({
        title: 'Bạn Có Chắc Chắn Xóa Toàn Bộ Dữ Liệu Danh Sách Này Sẽ Không Thể Khôi Phục Lại',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Tôi Chắc Chắn!',
        cancelButtonText: 'Để Sau'
    }).then((result) => {
        if (result.isConfirmed) {
            $.post('/dashboard/graph-api', {
                table: table,
                action: 'cleaner_table'
            }, function(data) {
                let status = data.status,
                    message = data.message;
                if (status == 'false') {
                    Swal.fire(
                        'Deleted!',
                        message,
                        'danger'
                    );
                    return
                }
                Swal.fire(
                    'Deleted!',
                    message,
                    'success'
                )
                location.reload();
            });
        }
    })
}
$("form[submit-ajax=ngockush]").submit(function(e) {
    e.preventDefault();
    var _this = this,
        table = $(_this).attr('table'),
        action = $(_this).attr('action'),
        table_id = $(_this).attr('id-table'),
        data = $(_this).serialize(),
        btn = $(_this).find("button[type=submit]");
    btn.prop('disabled', true);
    $.post(`/dashboard/graph-api?table=${table}&action=${action}&table_id=${table_id}`, data, function(res) {
        btn.prop('disabled', false);
        let status = res.status,
            message = res.message;
        if (status == false) {
            Swal.fire({
                title: "Thông Báo",
                html: message,
                icon: "warning"
            });
            return
        }
        Swal.fire({
            title: "Thông Báo",
            html: message,
            icon: "success"
        }).then((result) => {
            if (result.isConfirmed) {
                location.reload();
            }
        });
    });
});


$('.btn-upload').click(function() {
    var file_input = $(this).attr('data-input-id');
    $(file_input).click();
    $(file_input).change(function() {
        var _this = $(this),
            files = _this[0].files,
            image_input = _this.attr('data-result');
        if (files.length < 1) {
            return;
        }
        var upload_data = new FormData();
        upload_data.append('image', files[0]);
        $(`input[name=${image_input}]`).val(`Đang xử lý vui lòng chờ`).prop('readonly', true);
        _this.prop('disabled', true);
        $.ajax({
            url: '/dashboard/graph-api?action=upload-images',
            type: 'post',
            data: upload_data,
            contentType: false,
            processData: false,
            success: function(res) {
                _this.prop('disabled', false);
                const {
                    status,
                    message
                } = res;
                if (status == false) {
                    Swal.fire({
                        title: "Thông Báo",
                        html: message,
                        icon: "warning"
                    });
                    $(`input[name=${image_input}]`).val('').prop('readonly', false);
                    return
                }
                _this.hide();
                $(`input[name=${image_input}]`).val(message);
            },
            error: function() {
                $(`input[name=${image_input}]`).prop('readonly', false);
                $(`input[name=${image_input}]`).val('Tải lên không thành công');
                _this.prop('disabled', false);
            }
        });
    });
});

function createSlug(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // Trim leading and trailing whitespace
    str = str.toLowerCase();

    // Remove accents, swap ñ for n, etc
    var from = "àáãäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to = "aaaaaeeeeiiiioooouuuunc------";
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }
    str = str.replace(/[^a-z0-9 -]/g, '') // Remove invalid chars
        .replace(/\s+/g, '-') // Collapse whitespace and replace by -
        .replace(/-+/g, '-'); // Collapse dashes

    return str;
}
var convert_to_slug = function(_this) {
    var text = $(_this).val(),
        results = $(_this).attr('data-slug');
    $(results).val(text);
}
var to_day = () => {
    var date = new Date();
    var day = date.getDate().toString().padStart(2, '0');
    var month = (date.getMonth() + 1).toString().padStart(2, '0');
    var year = date.getFullYear();
    return `${day}/${month}/${year}`;
}

function generateRandomString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    var date = new Date();
    var options = {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit'
    };
    var formattedDate = date.toLocaleDateString('vi-VN', options);
    return result + btoa(encodeURIComponent(formattedDate));
}
$('.toggle-show-body').click(function() {
    var _this = $(this),
        data_show = _this.attr('data-show'),
        sv_body = $(data_show),
        icon = _this.find('i');
    if (_this.hasClass('active')) {
        _this.removeClass('active');
        $(icon).removeClass('fa-xmark').addClass('fa-plus');
    } else {
        _this.addClass('active');
        $(icon).removeClass('fa-plus').addClass('fa-xmark');
    }
    sv_body.toggle(function() {
        $('html, body').animate({
            scrollTop: sv_body.offset().top - 100
        }, 500);
    });
});
$('#checkAll').click(function() {
    $('.delete_id:checkbox').prop('checked', this.checked);
});
$('#detele-selected').click(function() {
    var num_delete = 0,
        _this = $(this),
        table = _this.attr('data-table');
    var myCheckboxes = new Array();
    $('.delete_id:checked').each(function() {
        myCheckboxes.push($(this).val());
        num_delete++;
    });
    Swal.fire({
        title: `Bạn có chắc chắn xóa ${num_delete} trường này không`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Tôi Chắc Chắn!',
        cancelButtonText: 'Để Sau'
    }).then((result) => {
        if (result.isConfirmed) {
            $.post(`/dashboard/graph-api?action=delete-checkbox`, {
                myCheckboxes,
                table
            }, function(res) {
                let status = res.status,
                    message = res.message;
                if (status == false) {
                    Swal.fire({
                        title: "Thông Báo",
                        html: message,
                        icon: "warning"
                    });
                    return
                }
                Swal.fire({
                    title: "Thông Báo",
                    html: message,
                    icon: "success"
                }).then((result) => {
                    if (result.isConfirmed) {
                        location.reload();
                    }
                });
            });
        }
    });
});

$('#clear-cache-site').click(function() {
    $.post(`/dashboard/graph-api?action=clear-cache-site`, function(res) {
        const status = res.status,
            message = res.message;
        if (status == false) {
            Swal.fire({
                title: "Thông Báo",
                html: message,
                icon: "warning"
            });
            return
        }
        Swal.fire({
            title: "Thông Báo",
            html: message,
            icon: "success"
        });
    });
});
$('.remove-server').click(async function() {
    const remove_key = $(this).attr('data-remove');
    const episodeId = $(this).attr('episode-id');
    $(this).prop('disabled', true).html('Đang xử lý...');
    const {
        status,
        message
    } = await $.post(`/dashboard/graph-api?action=removeServer`, {
        remove_key,
        episodeId
    });
    $(`#server_${remove_key}`).remove();
});
$('.removeObject').click(function() {
    var _this = $(this),
        key = _this.attr('data-key'),
        object_data = _this.attr('data-ob');
    $.post(`/dashboard/graph-api?action=removeObject`, {
        key,
        object_data
    }).done(function(res) {
        const {
            status,
            message
        } = res;
        if (status == false) {
            Swal.fire({
                title: "Thông Báo",
                html: message,
                icon: "warning"
            });
            return
        }
        Swal.fire({
            title: "Thông Báo",
            html: message,
            icon: "success"
        }).then((result) => {
            location.reload();
        });
    });
});
$('.select_page').change(function() {
    var _this = $(this),
        pageType = _this.val();
    $('input[name=pageType]').val(pageType);
    $('form[submit-ajax=ngockush]').show();
    Swal.fire({
        title: "Đang tải ....."
    })
    $.post(`/dashboard/graph-api?action=loadPageType`, {
        pageType
    }).done(function(res) {
        Swal.close();
        const {
            status,
            message
        } = res;
        if (status == false) {
            Swal.fire({
                title: "Thông Báo",
                html: message,
                icon: "warning"
            });
            return
        }
        $('input[name=title]').val(message.title);
        $('input[name=description]').val(message.description);
        $('input[name=keywords]').val(message.keyword);
        $('#bbcode').html(message.bbcode);
    });
});

$('input[name=season_news]').on("keydown", function(event) {
    if (event.which == 13) {
        var seasonNum = $(this).val(),
            movieId = $(this).attr('data-movie');
        $.post(`/dashboard/graph-api?action=add_news_season`, {
            seasonNum,
            movieId
        }).done(function(res) {
            const {
                status,
                message
            } = res;
            if (status == false) {
                Swal.fire({
                    title: "Thông Báo",
                    html: message,
                    icon: "warning"
                });
                return
            }
            Swal.fire({
                title: "Thông Báo",
                html: message,
                icon: "success"
            }).then((result) => {
                location.reload();
            });
        });
    }

});