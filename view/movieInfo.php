<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$movieSlug = $st[0] ? sql_escape($st[0]) : header('location: /');
if (get_total('movie', "WHERE slug = '$movieSlug'") <= 0) header('location: /');
$info = GetDataArr('movie', "slug = '$movieSlug'");
$episode_id = $st[0] ? (int)explode('ep-', $st[1])[1] : NULL;
if (!$episode_id) {
    $episode_id = get_data_multi('id', "episode", "movieId = {$info['id']}");
}
$setting = [
    "pageType" => "Thông tin phim",
    "movie_name" => $info['name'],
    "movie_year" => $info['year'],
    "movie_country" => $info['country'],
    "movie_quanlity" => $info['quality'],
    "movie_orther_name" => $info['origin_name']
];
$avd = [
    "a" => BBcode($setting)['title'],
    "b" => BBcode($setting)['description'],
    "c" => BBcode($setting)['keywords'],
    "d" => URL_LOAD,
    "e" => $info['thumb']
];
?>
<!DOCTYPE html>
<html data-aid="iptfilm">

<head>
    <?php require_once(_DIR . '/require/head.php'); ?>
</head>

<body>
<?php require_once(_DIR . '/require/header.php'); ?>
<div class="container">
    <div id="wrapper">
        <div id="body">
            <script>
                var site_config = {
                    domain: '<?= HOME ?>',
                    disqus: '<?= $cf['disqusEmbed'] ?>',
                    page: 'movie_watch'
                };

                var movie = {
                    id: "<?= $info['id'] ?>",
                    shortlink: "<?= base_url("/anime/$movieSlug") ?>",
                };
                var episode_play = {
                    id: "<?= $episode_id ?>",
                    shortlink: "<?= base_url("/anime/$movieSlug/ep-$episode_id") ?>"
                };
            </script>
            <div id="watch-page" data-id="<?= $info['id'] ?>" data-url="<?= URL_LOAD ?>"
                 data-ep-name="<?= $episode_id ?>">
                <div class="aside-wrapper">

                    <aside class="main">
                        <div class="alert mb-3"
                             style="background: rgb(255, 170, 0); color: rgb(17, 17, 17); font-size: 16px; font-weight: 600;">
                            <?= un_htmlchars($cf['topNotice']) ?>
                        </div>
                        <div id="player-wrapper">

                            <div class="player-frame">
                                <div id="player">
                                    <div class="backdrop"
                                         style="background-image:url('<?= $info['coverUrl'] ?? $info['thumb'] ?>')">
                                    </div>
                                    <div class="play">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="alert-player"></div>
                        <?php /*<div id="player-controls">
                                <div class="left">

                                    <div class="ctrl light" data-default="1">
                                        <i class="fas fa-lightbulb"></i> Light
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="ctrl forward prev">
                                        <i class="fas fa-step-backward"></i> Prev
                                    </div>
                                    <div class="ctrl forward next">
                                        <i class="fas fa-step-forward"></i> Next
                                    </div>

                                </div>
                            </div>
                        */ ?>
                        <div id="episodes"></div>
                        <div class="clearfix">
                        </div>
                        <section id="w-info">
                            <div class="poster">
                                <div>
                                    <img src="<?= $info['thumb'] ?>" alt="<?= $info['name'] ?>">
                                </div>
                            </div>
                            <div class="info">
                                <h1 class="title d-title" data-jp="<?= $info['name'] ?>"><?= $info['name'] ?></h1>
                                <div class="alias"><?= $info['origin_name'] ?? $info['name'] ?></div>
                                <div class="synopsis">
                                    <div class="content">
                                        <p><?= $info['content'] ?></p>
                                    </div>

                                </div>
                                <div class="bl-meta">
                                    <div class="meta">
                                        <div>
                                            Type: <span><?= $info['type'] ?></span>
                                        </div>
                                        <div>
                                            Date aired: <span> <?= $info['year'] ?> </span>
                                        </div>

                                        <div>
                                            Status: <span><?= ucfirst($info['status']) ?></span>
                                        </div>
                                        <div>
                                            Genre: <span>
                                                    <?= json_to_href($info['genres'], "/genre/", 'genres') ?>
                                                </span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                        <div class="addthis_inline_share_toolbox">
                        </div>

                        <br/>
                        <section id="comments" data-src="9anime-5udfmqkfuk.disqus.com" data-id="81kH" data-load="true"
                                 data-link="<?= base_url("/anime/$movieSlug") ?>">
                            <div class="head with-more">
                                <div class="title">
                                    Comments
                                </div>

                            </div>
                            <div class="body">
                                <div id="disqus_thread">
                                </div>

                            </div>
                        </section>
                    </aside>
                    <?php require_once(_DIR . '/require/sidebar.php'); ?>
                </div>
            </div>
        </div>
        <?php require_once(_DIR . '/require/foot.php'); ?>
    </div>
</div>
<?php require_once(_DIR . '/require/tempJs.php'); ?>
<div id="mask-overlay"></div>
</body>

</html>