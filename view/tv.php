<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$page = ($_GET['page'] ? (int)$_GET['page'] : 1);
$avd = [
    "a" => BBcode(["pageType" => "Phim bộ"])['title'],
    "b" => BBcode(["pageType" => "Phim bộ"])['description'],
    "c" => BBcode(["pageType" => "Phim bộ"])['keywords'],
    "d" => URL_LOAD
];
$limit = 32;
$paging = page_checker('movie', "WHERE public >= 1 AND type = 'tv'", $limit, $page);
$movieList = $paging['total'] >= 1 ? showMovie("WHERE public >= 1 AND type = 'tv' ORDER BY created DESC LIMIT {$paging['start']},$limit") : '';
?>
<!DOCTYPE html>
<html data-aid="iptfilm">

<head>
    <?php require_once(_DIR . '/require/head.php'); ?>
</head>

<body>
<?php require_once(_DIR . '/require/header.php'); ?>
<div class="container">
    <div id="wrapper">
        <div id="body">
            <div class="list-page full-page">
                <section>
                    <div class="head">
                        <h1 class="title"><?= web_name() ?> WATCH TV SHOW ONLINE FREE WITH DUB AND SUB LATEST UPDATE</h1>
                    </div>
                    <div class="ani items">
                        <?= $movieList ?>
                    </div>
                    <?= movie_navition($paging['total'], $limit, $page, base_url("/tv?page=")) ?>
                </section>
            </div>
        </div>
        <?php require_once(_DIR . '/require/foot.php'); ?>
    </div>
</div>
<?php require_once(_DIR . '/require/tempJs.php'); ?>
</body>