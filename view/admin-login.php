<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
if ($_SESSION['auth']) header("Location:" . base_admin());
?>
<!DOCTYPE html>
<html data-aid="iptfilm">

<head>
    <?php require_once(_DIR . '/require/head.php'); ?>
</head>

<body>
    <div class="container">
        <div id="wrapper">
            <?php require_once(_DIR . '/require/header.php'); ?>
            <div id="body">
                <div class="list-page full-page">
                    <section>
                        <div class="head">
                            <h1 class="title">Đăng nhập tài khoản admin <?= web_name() ?></h1>
                        </div>
                        <form class="ani items" id="login-admin">
                            <div class="col-lg-12 form-group">
                                <input type="text" class="form-control" name="username" placeholder="Tên tài khoản admin">
                            </div>
                            <div class="col-lg-12 form-group">
                                <input type="text" class="form-control" name="password" placeholder="Mật khẩu tài khoản admin">
                            </div>
                            <div class="col-lg-12 form-group text-center">
                                <button type="submit" class="btn btn-primary btn-sm">Đăng nhập</button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
            <?php require_once(_DIR . '/require/foot.php'); ?>
        </div>
    </div>
    <?php require_once(_DIR . '/require/tempJs.php'); ?>
    <script>
        $('#login-admin').submit(async function(e) {
            e.preventDefault();
            const data = $(this).serialize();
            const {
                status,
                message
            } = await $.post('/ajax/users/admin-login', data);
            if (status == false) {
                alert(message);
                return;
            }
            window.location.reload();
        });
    </script>
</body>