<?php
header('Content-Type: application/xml; charset=utf-8');
echo '<?xml version="1.0" encoding="utf-8"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<url>
<loc>' . HOME . '</loc>
<changefreq>daily</changefreq>
<priority>1.0</priority>
<lastmod>' . date('Y-m-d') . 'T' . date('H:m:i+07:00') . '</lastmod>
</url>
';
$movie = $mysql->query("SELECT * FROM " . DATABASE_FX . "movie WHERE public >= 1 ORDER BY time DESC");
while ($row = $movie->fetch(PDO::FETCH_ASSOC)) {
    echo "
<url>
<loc>" . base_url("/movie/{$row['slug']}") . "</loc>
<changefreq>yearly</changefreq>
<priority>0.8</priority>
<lastmod>" . date('Y-m-d') . "T" . date('H:m:i+07:00') . "</lastmod>
</url>
";
}
$cate = $mysql->query("SELECT * FROM " . DATABASE_FX . "genres ORDER BY id DESC");
while ($row = $cate->fetch(PDO::FETCH_ASSOC)) {
    echo "
<url>
<loc>" . base_url("/genre/{$row['slug']}") . "</loc>
<changefreq>yearly</changefreq>
<priority>0.8</priority>
<lastmod>" . date('Y-m-d') . "T" . date('H:m:i+07:00') . "</lastmod>
</url>
";
}
echo '</urlset>';
