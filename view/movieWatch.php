<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$movieSlug = $st['movie_slug'] ? $st['movie_slug'] : header('location: /');
$ep_slug = $st['ep_slug'] ? $st['ep_slug'] : header('location: /');

if (get_total('movie', "WHERE slug = '$movieSlug'") <= 0) header('location: /');
$info = GetDataArr('movie', "slug = '$movieSlug'");
if (!$_SESSION["view_{$info['id']}"]) {
    $mysql->update('movie', "view = view + 1", "id = {$info['id']}");
    $_SESSION["view_{$info['id']}"] = 'SuccessViewer';
}
if (get_total('episode', "WHERE movieId = {$info['id']}") <= 0) header('location: /');
$seasonMovie = jsonDecode(get_data_multi('data', 'episode', "movieId = {$info['id']}"));
$ext = explode('-', $ep_slug);
$season = (int)$ext[0];
$episode = (int)$ext[1];
$sv = (int)$_GET['sv'];
$setting = [
    "pageType" => "Xem phim",
    "movie_name" => $info['name'],
    "movie_year" => $info['year'],
    "movie_country" => $info['country'],
    "movie_quanlity" => $info['quality'],
    "movie_orther_name" => $info['origin_name']
];
$avd = [
    "a" => BBcode($setting)['title'],
    "b" => BBcode($setting)['description'],
    "c" => BBcode($setting)['keywords'],
    "d" => URL_LOAD,
    "e" => $info['thumb']
];
$movieRate = comicRate($info['votePoint'], $info['voteNum']);
historyWatching($info['id']);
$episode_name = $seasonMovie[($season - 1)]['season_episode'][($episode - 1)]['episode_name'];

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <?php
    require_once(_DIR . '/require/head.php');
    ?>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/gh/ngockush/fastplayer@main/jwplayer.js"></script>
    <script type="text/javascript">
        jwplayer.key = "cLGMn8T20tGvW+0eXPhq4NNmLB57TrscPjd1IyJF84o=";
    </script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/gh/ngockush/fastplayer@main/jwplayer.core.controls.html5.js"></script>
    <style>
        #WatchPlayer {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }
    </style>
</head>

<body class="watch" data-id="<?= $info['id'] ?>" data-season="<?= $season ?>" data-ep="<?= $episode ?>" data-sv="<?= $sv ?>" episode-num="<?= count($seasonMovie[($season - 1)]['season_episode']) ?>" data-link="<?= $info['slug'] ?>">
    <div class="wrapper">
        <?php require_once(_DIR . '/require/header.php'); ?>
        <main class="">
            <?= quangCao('Đầu trang xem phim') ?>
            <div class="watch-wrap" itemprop="mainEntity" itemscope itemtype="https://schema.org/Movie">
                <div id="film-player" class="playable">
                    <div class="film-background " style="background-image: url('<?= ($info['coverUrl'] ? $info['coverUrl'] : $info['thumb']) ?>')"></div>
                    <div class="alert mb-3" style="background: rgb(255, 170, 0); color: rgb(17, 17, 17); font-size: 16px; font-weight: 600;">
                        <?= un_htmlchars($cf['topNotice']) ?>
                    </div>
                    <div style="z-index: 999;">
                        <?= quangCao('Trên player trang xem phim') ?>
                    </div>
                    <div class="container big">
                        <div class="film-container">
                            <div id="player">
                                <div class="btn-play"> <i class="fa-solid fa-play"></i> </div>
                            </div>
                        </div>
                        <div id="film-controls">
                            <div class="film-controls-wrap">
                                <div class="film-control movies-previous"><i class="fa-solid fa-circle-left"></i> Previous</div>
                                <div class="film-control movies-next"><i class="fa-solid fa-circle-right"></i> Next</div>
                                <div class="light film-control"><i class="fa-regular fa-lightbulb"></i><span>Light</span></div>
                                <div class="film-control add-favorite"><i class="fa-regular fa-bookmark"></i> <span>Add bookmark</span></div>
                                <div class="film-control" data-toggle="modal" data-target="#report"><i class="fa-solid fa-triangle-exclamation"></i> <span>Report</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container big order-1">
                    <div id="film-servers">
                        <div class="text-muted text-center server-notice mb-2"> If current server doesn't work please try other servers below. </div>
                        <div class="film-servers-wrap">
                            <?php if (count($seasonMovie) >= 1) {
                                foreach ($seasonMovie[($season - 1)]['season_episode'][($episode - 1)]['episode_server'] as $k => $v) { ?>
                                    <a class="film-server <?= (($k + 1) == $sv ? 'active' : '') ?>" href="javascript:;" data-sv="<?= ($k + 1) ?>">
                                        <div> <span><i class="fa-solid fa-server"></i> Server</span>
                                            <div><?= $v['server_name'] ?></div>
                                        </div>
                                    </a>
                            <?php }
                            } ?>
                        </div>
                    </div>
                    <?= quangCao('Giữa trang xem phim') ?>
                    <div class="site-wrap swap">
                        <div class="site-content">
                            <div class="zone" id="film-detail">

                                <div class="film-poster"> <span> <img itemprop="image" src="<?= $info['thumb'] ?>" alt="<?= $info['name'] ?>"> </span> </div>
                                <div class="film-info">
                                    <h1 itemprop="name" class="film-title"><?= $info['name'] ?></h1>
                                    <div class="film-subtitle">
                                        <span class="quality"><?= $info['quality'] ?></span>
                                        <span><i class="fa-solid fa-star"></i> <?= $info['imdb'] ?></span>
                                        <span><?= $info['duration'] ?></span>
                                    </div>
                                    <div class="film-desc cts-wrapper"><?= strip_tags($info['content']) ?></div>
                                    <div class="film-meta">
                                        <div>
                                            <div>Type:</div> <span>
                                                <?php if ($info['type'] == 'single') { ?>
                                                    <a href="/movie">Movie</a>
                                                <?php } else if ($info['type'] == 'series') { ?>
                                                    <a href="/tv-show">TV Shows</a>
                                                <?php } ?>
                                            </span>
                                        </div>
                                        <?php if ($info['country']) { ?>
                                            <div>
                                                <div>Country:</div> <span>
                                                    <?= json_to_href($info['country'], "/country/", 'country') ?>
                                                </span>
                                            </div>
                                        <?php } ?>
                                        <?php if ($info['genres']) { ?>
                                            <div>
                                                <div>Genre:</div> <span>
                                                    <?= json_to_href($info['genres'], "/genre/", 'genres') ?>
                                                </span>
                                            </div>
                                        <?php } ?>
                                        <?php if ($info['year']) { ?>
                                            <div>
                                                <div>Year:</div> <span itemprop="dateCreated"><?= $info['year'] ?></span>
                                            </div>
                                        <?php } ?>
                                        <?php if ($info['Production']) { ?>
                                            <div>
                                                <div>Production:</div> <span> <?= json_to_href($info['Production'], '/production/', false) ?></span>
                                            </div>
                                        <?php } ?>
                                        <?php if ($info['Casts']) { ?>
                                            <div>
                                                <div>Cast:</div> <span>
                                                    <?= json_to_href($info['Casts'], '/cast/', false) ?>
                                                </span>
                                            </div>
                                        <?php } ?>
                                        <div class="tags">
                                            <div>Tags:</div> <span>
                                                <h2><?= $info['name'] ?> free stream</h2>, <h2><?= $info['name'] ?> hd download</h2>, <h2>free watch <?= $info['name'] ?></h2>, <h2><?= $info['name'] ?> online watch</h2>, <h2><?= $info['name'] ?> watch free online</h2>, <h2><?= $info['name'] ?> streaming online</h2>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        if ($info['type'] == 'series') {
                            if (count($seasonMovie) >= 1) {
                                $season_name = $seasonMovie[($season - 1)]['season_name'];
                        ?>
                                <div class="site-sidebar">
                                    <div class="zone tv" id="film-episodes">
                                        <div class="ep-head">
                                            <div class="dropdown">
                                                <button class="btn dropdown-toggle" data-toggle="dropdown" data-placeholder="false" href="#">
                                                    <i class="fa-solid fa-clapperboard"></i> <span class="season-view"> <?= $season_name ?> </span>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <?php foreach ($seasonMovie as $k => $v) { ?>
                                                        <a class="dropdown-item season-item" href="javascript:;" data-season="<?= ($k + 1) ?>"> <?= $v['season_name'] ?> </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ep-body">
                                            <ul class="episodes" style="display: block">
                                                <?php foreach ($seasonMovie[($season - 1)]['season_episode'] as $k1 => $v1) { ?>
                                                    <li>
                                                        <a href="javascript:;" class="episode-change <?= (($k1 + 1) == $episode ? 'active' : '') ?>" data-episode="<?= ($k1 + 1) ?>">
                                                            <span class="num">Episode <?= ($k1 + 1) ?>:</span>
                                                            <span><?= $v1['episode_name'] ?></span>
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                        <div class="ep-foot">
                                            <div class="ep-btn"> <span>Go to episode</span>
                                                <form class="d-flex goToEpisode" autocomplete="off">
                                                    <input class="form-control" type="text" placeholder="<?= $episode ?>">
                                                    <button type="submit" class="btn btn-primary"><i class="fa-solid fa-caret-right"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        <?php }
                        } ?>

                    </div>
                    <div class="site-wrap">
                        <div class="site-content">
                            <div class="zone" id="comment" data-src="<?= $cf['disqusEmbed'] ?>" data-id="<?= web_name() ?>_<?= $info['id'] ?>" data-url="<?= base_url("/movie/{$info['slug']}") ?>" data-load="true">
                                <div class="zone-head">
                                    <div class="zone-start">
                                        <h2 class="zone-title"><i class="fa-solid fa-comment-dots"></i> Comment</h2>
                                    </div>
                                </div>
                                <div class="body">
                                    <div id="disqus_thread"></div>
                                </div>
                            </div>
                            <?= quangCao('Cuối trang xem phim') ?>
                        </div>
                        <div class="site-sidebar">
                            <div class="zone">
                                <div class="zone-head mb-3">
                                    <div class="zone-start justify-content-between w-100">
                                        <h2 class="zone-title"><i class="fa-solid fa-clapperboard"></i> You may also like</h2>
                                    </div>
                                </div>
                                <div class="basic head films">
                                    <?= showMovieSidebar("WHERE public >= 1 AND type = '{$info['type']}' AND id != {$info['id']} ORDER BY RAND() LIMIT 16"); ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal fade" id="report">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="close" data-dismiss="modal"><i class="fa-solid fa-circle-xmark"></i></div>
                        <h2>Report an Issue</h2>
                        <div class="text-muted mt-2 mb-2">Please let us know what's wrong so we can fix it as soon as possible.</div>
                        <form class="movie_report" method="post">
                            <input type="text" name="movieId" style="display: none;" value="<?= $info['id'] ?>">
                            <h5><?= $info['name'] ?></h5></span>
                            <div class="row mt-3">
                                <div class="col">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="video_wrong" name="issue[]" value="video_wrong">
                                        <label class="custom-control-label" for="video_wrong">Wrong video</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="audio_sync" name="issue[]" value="audio_sync">
                                        <label class="custom-control-label" for="audio_sync">Audio not synced</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="sub_sync" name="issue[]" value="sub_sync">
                                        <label class="custom-control-label" for="sub_sync">Subtitle not synced</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="skiptime_wrong" name="issue[]" value="skiptime_wrong">
                                        <label class="custom-control-label" for="skiptime_wrong">Wrong skiptime</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="mt-3 mb-1">Other:</label>
                                <textarea class="w-100 form-control" name="message" rows="3"></textarea>
                            </div>
                            <div class="form-group d-flex justify-content-center"> <span class="captcha g-recaptcha" data-theme="dark"></span> </div>
                            <button type="submit" class="submit btn-primary btn btn-lg w-100">Send report</button>
                            <div class="loading" style="display: none;"></div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
        <?php require_once(_DIR . '/require/foot.php'); ?>
    </div>
    <div class="modal fade" id="sign">
        <div class="modal-dialog modal-dialog-centered cts-wrapper">
            <div class="modal-content cts-block" data-name="signin">
                <div class="close" data-dismiss="modal"><i class="fa-solid fa-circle-xmark"></i></div>
                <h3 class="text-major">Sign in to continue</h3>
                <div class="text-muted mt-2">Don't have an account? <a href="#" class="cts-switcher" data-target="signup">Create new account</a>
                    <br>It takes less than a minute
                </div>
                <form class="mt-3 ajax-login" action="ajax/user/login" method="post" data-broadcast="user:updated">
                    <div class="alert alert-danger" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert"><b>×</b></button>
                        <span></span>
                    </div>
                    <div class="form-group md">
                        <input type="text" class="form-control form-control-lg" name="email" placeholder="Email" required>
                    </div>
                    <div class="form-group md">
                        <input type="password" class="form-control form-control-lg" placeholder="Your Password" name="password" placeholder="Your password" required>
                    </div>

                    <button type="submit" class="btn-primary btn-lg btn mt-3 w-100 submit">Sign In <i class="fa-sharp fa-solid fa-circle-chevron-right"></i></button>
                    <div class="loading" style="display: none;"></div>
                </form>
            </div>
            <div class="modal-content cts-block" data-name="signup" style="display:none">
                <div class="close" data-dismiss="modal"><i class="fa-solid fa-circle-xmark"></i></div>
                <h3 class="text-major">Create new account</h3>
                <div class="text-muted mt-2">Create an account to use all the features..</div>
                <form class="mt-3 ajax-register" action="ajax/user/register" method="post" data-broadcast="user:updated">
                    <div class="alert alert-danger" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert"><b>×</b></button>
                        <span></span>
                    </div>
                    <div class="form-group"> <input type="text" class="form-control form-control-lg" placeholder="Fullname" name="fullname" required> </div>
                    <div class="form-group"> <input type="email" class="form-control form-control-lg" placeholder="Email" name="email" required> </div>
                    <div class="form-group"> <input type="password" class="form-control form-control-lg" placeholder="Password" name="password" required> </div>
                    <div class="form-group"> <input type="password" class="form-control form-control-lg" placeholder="Password" name="password_confirmation" required> </div>
                    <div class="form-group d-flex justify-content-center"> <span class="captcha g-recaptcha" data-theme="dark"></span> </div>
                    <button type="submit" class="btn-primary btn btn-lg w-100 mt-3 submit">Sign Up <i class="fa-sharp fa-solid fa-circle-chevron-right"></i></button>
                    <div class="loading" style="display: none;"></div>
                </form>
                <div class="text-muted text-center mt-3">Already have an account? <a href="#" class="cts-switcher" data-target="signin">Sign In</a></div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="request">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="close" data-dismiss="modal"><i class="fa-solid fa-circle-xmark"></i></div>
                <h3 class="text-major">Send a Request</h3>
                <div class="text-muted mt-2">If you can't find your favourite movie/tv-show in our library, please submit a request. We will try to make it available as soon as possible.</div>
                <form class="mt-3 ajax" action="ajax/film/request" method="post">
                    <div class="form-group"> <input type="text" class="form-control form-control-lg" placeholder="Movie/TV name" name="title"> </div>
                    <div class="form-group"> <input type="text" class="form-control form-control-lg" placeholder="Enter link here" name="ref_url"> </div>
                    <div class="form-group"> <textarea rows="3" class="form-control form-control-lg" name="detail" placeholder="More details about it if possible"></textarea> </div>
                    <div class="form-group d-flex justify-content-center"> <span class="captcha g-recaptcha" data-theme="dark"></span> </div>
                    <div class="form-group d-flex justify-content-center"> <span class="captcha" data-theme="dark"></span> </div> <button class="submit btn-primary btn btn-lg mt-3 w-100">Send Request <i class="fa-sharp fa-solid fa-circle-chevron-right"></i></button>
                </form>
            </div>
        </div>
    </div>
    <script>
        var recaptchaSiteKey = '<?= $cf['captchaKey'] ?>';
    </script>
    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.1.1/lazysizes.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser-polyfill.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.1/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/4.0.0/js/tooltipster.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/5.4.4/js/swiper.min.js"></script>
    <script type="text/javascript" src="<?= HOME ?>/assets/t11/min/all.js"></script>
    <script type="text/javascript" src="<?= HOME ?>/assets/t11/min/movie_watch.js"></script>
    <div class="light-dark" style="width: 100%; height: 100%; position: fixed; left: 0px; top: 0px; z-index: 43046721; background: rgb(0, 0, 0); opacity: 0.95; display: none;"></div>
    <div id="toast"></div>
</body>

</html>