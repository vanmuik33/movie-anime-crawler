<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
?>

<!DOCTYPE html>
<html data-aid="iptfilm">

<head>
    <?php require_once(_DIR . '/require/head.php'); ?>
</head>

<body>
<?php require_once(_DIR . '/require/header.php'); ?>
    <div class="container">
        <div id="wrapper">
            <div id="body">
                <section id="hotest" class="swiper-container">
                    <div class="items swiper-wrapper">
                        <?php
                        $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "movie WHERE onSlider >= 1 ORDER BY created DESC LIMIT 10");
                        while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
                            $episode_id = get_data_multi('id', "episode", "movieId = {$row['id']}");
                        ?>
                            <div class="swiper-slide item" style="background-image: url('<?= $row['coverUrl'] ? $row['coverUrl'] : $row['thumb'] ?>');">
                                <a class="inner" href="<?= base_url("/anime/{$row['slug']}/ep-$episode_id") ?>">
                                    <div class="content">
                                        <div class="genre"><?= categorySpan($row['genres']) ?></div>
                                        <div class="title d-title" data-jp="<?= $row['name'] ?>"><?= $row['name'] ?></div>
                                    </div>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="swiper-pagination"></div>
                </section>
                <div class="aside-wrapper">
                    <aside class="main">
                        <section id="recent-update">
                            <div class="head with-more with-tabs">
                                <div class="start">
                                    <h2 class="title">Recently Updated</h2>
                                </div>
                            </div>
                            <div class="body">
                                <div class="ani items">
                                    <?= showEpisode("WHERE public >= 1 AND table_episode.id = (SELECT MAX(id) FROM table_episode AS sub WHERE sub.movieId = table_movie.id) ORDER BY epId DESC LIMIT 30"); ?>
                                </div>
                            </div>
                        </section>
                        <section id="trending">
                            <div class="head with-more with-tabs">
                                <div class="start">
                                    <h2 class="title">Trending</h2>
                                </div>
                            </div>
                            <div class="body">
                                <div class="ani items">
                                    <?= showMovie("WHERE public >= 1 ORDER BY view DESC LIMIT 30"); ?>
                                </div>
                            </div>
                        </section>


                    </aside>
                    <?php require_once(_DIR . '/require/sidebar.php'); ?>
                </div>
            </div>
            <?php require_once(_DIR . '/require/foot.php'); ?>
        </div>
    </div>
    <?php require_once(_DIR . '/require/tempJs.php'); ?>
</body>

</html>