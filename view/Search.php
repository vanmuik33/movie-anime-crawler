<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$keyword = $st['keyword'] ? sql_escape($st['keyword']) : "";
if (!$keyword) header('location: /');
$keyLink = chuyenslug($keyword);
$orders = "AND name LIKE '%$keyword%' OR origin_name LIKE '%$keyword%' OR slug LIKE '%$keyLink%'";
$page = ($_GET['page'] ? (int)$_GET['page'] : 1);
$avd = [
    "a" => BBcode(["pageType" => "Tìm kiếm", "keywords" => $keyword])['title'],
    "b" => BBcode(["pageType" => "Tìm kiếm", "keywords" => $keyword])['description'],
    "c" => BBcode(["pageType" => "Tìm kiếm", "keywords" => $keyword])['keywords'],
    "d" => URL_LOAD
];
$limit = 32;
$paging = page_checker('movie', "WHERE public >= 1 $orders", $limit, $page);
$movieList = $paging['total'] >= 1 ? showMovie("WHERE public >= 1 $orders ORDER BY time DESC LIMIT {$paging['start']},$limit") : '';
?>
<!DOCTYPE html>
<html>

<head>
    <?php require_once(_DIR . '/require/head.php'); ?>
</head>

<body class="">
    <div class="wrapper">
        <?php require_once(_DIR . '/require/header.php'); ?>
        <main>

            <div class="container">
                <div class="aside-wrapper">
                    <aside class="main">
                        <section>
                            <?= quangCao('Đầu trang') ?>
                            <div class="head">
                                <div class="start">
                                    <h2 class="title"><i class="bi bi-file-play-fill"></i>Search results for "<?= $keyword ?>"</h2>
                                </div>
                            </div>
                            <?php require_once(_DIR . '/require/filter-form.php') ?>
                            <div class="clearfix"></div>
                            <div class="movies items ">
                                <?= $movieList ?>
                            </div>
                            <?= movie_navition($paging['total'], $limit, $page, base_url("/cast/$name?page=")) ?>
                            <?= quangCao('Cuối trang') ?>
                        </section>
                    </aside>
                    <aside class="sidebar">
                        <section>
                            <div class="head">
                                <div class="start">
                                    <h2 class="title"><i class="bi bi-file-play-fill"></i>Suggestions</h2>
                                </div>
                            </div>
                            <div class="body">
                                <div class="scaff top9 related items">
                                    <?php
                                    $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "movie ORDER BY RAND() LIMIT 20");
                                    while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                        <a class="item" href="<?= base_url("/movie/{$row['slug']}") ?>">
                                            <div class="poster" data-tip="62298?/cachee045">
                                                <div><img class="lazyload" data-src="<?= $row['thumb'] ?>" alt="<?= $row['name'] ?>"></div>
                                            </div>
                                            <div class="info">
                                                <div>
                                                    <?php if ($row['type'] == 'single') { ?>
                                                        <span><?= $row['year'] ?></span>
                                                        <span>Movie</span>
                                                        <span><?= $row['duration'] ?></span>
                                                    <?php } else {
                                                        $seasonId = get_data_multi('id', "season", "movieId = {$row['id']} ORDER BY num DESC");
                                                    ?>
                                                        <span><?= ($row['type'] == 'single' ? 'Movie' : 'TV Shows') ?></span>
                                                        <span>SS <?= get_data_multi('num', "season", "id = $seasonId") ?></span>
                                                        <span>EP <?= get_data_multi('num', "episode", "seasonId = $seasonId ORDER BY num DESC") ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="name"><?= $row['name'] ?></div>
                                            </div>
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>
                        </section>
                    </aside>
                </div>
            </div>
        </main>
        <?php require_once(_DIR . '/require/foot.php'); ?>
    </div>
    <?php require_once(_DIR . '/require/tempJs.php'); ?>
</body>

</html>