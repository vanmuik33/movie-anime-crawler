<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$genre_links = $st[0] ? $st[0] : header('location: /');
if (get_total('genres', "WHERE slug = '$genre_links'") <= 0) header('location: /');
$orders = "AND genres LIKE '%\"$genre_links\"%'";
$data = GetDataArr('genres', "slug = '$genre_links'");
$page = ($_GET['page'] ? (int)$_GET['page'] : 1);
$avd = [
    "a" => BBcode(["pageType" => "Thể loại", "name_genres" => $data['name']])['title'],
    "b" => BBcode(["pageType" => "Thể loại", "name_genres" => $data['name']])['description'],
    "c" => BBcode(["pageType" => "Thể loại", "name_genres" => $data['name']])['keywords'],
    "d" => URL_LOAD
];
$limit = 32;
$paging = page_checker('movie', "WHERE public >= 1 $orders", $limit, $page);
$movieList = $paging['total'] >= 1 ? showMovie("WHERE public >= 1 $orders ORDER BY created DESC LIMIT {$paging['start']},$limit") : '';
?>
<!DOCTYPE html>
<html data-aid="iptfilm">

<head>
    <?php require_once(_DIR . '/require/head.php'); ?>
</head>

<body>
<?php require_once(_DIR . '/require/header.php'); ?>
<div class="container">
    <div id="wrapper">
        <div id="body">
            <div class="list-page full-page">
                <section>
                    <div class="head">
                        <h1 class="title">WATCH <?= web_name() ?> <?= $data['name'] ?> FREE ANIME ONLINE STREAMING</h1>
                    </div>
                    <div class="ani items">
                        <?= $movieList ?>
                    </div>
                    <?= movie_navition($paging['total'], $limit, $page, base_url("/genre/$genre_links?page=")) ?>
                </section>
            </div>
        </div>
        <?php require_once(_DIR . '/require/foot.php'); ?>
    </div>
</div>
<?php require_once(_DIR . '/require/tempJs.php'); ?>
</body>