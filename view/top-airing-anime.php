<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$page = ($_GET['page'] ? (int)$_GET['page'] : 1);
$avd = [
    "a" => BBcode(["pageType" => "Top airing"])['title'],
    "b" => BBcode(["pageType" => "Top airing"])['description'],
    "c" => BBcode(["pageType" => "Top airing"])['keywords'],
    "d" => URL_LOAD
];
$limit = 24;
$paging = page_checker('movie', "WHERE public >= 1", $limit, $page);
$movieList = $paging['total'] >= 1 ? showMovie("WHERE public >= 1 ORDER BY view DESC LIMIT {$paging['start']},$limit") : '';
?>

<!DOCTYPE html>
<html data-aid="iptfilm">

<head>
    <?php require_once(_DIR . '/require/head.php'); ?>
</head>

<body>
    <div class="container">
        <div id="wrapper">
            <?php require_once(_DIR . '/require/header.php'); ?>
            <div id="body">
                <div class="list-page full-page">
                    <section>
                        <div class="head">
                            <h1 class="title"><?= web_name() ?> List Anime Top Airing Series TV Online Free With DUB and SUB latest update</h1>
                        </div>
                        <div class="ani items">
                            <?= $movieList ?>
                        </div>
                        <?= movie_navition($paging['total'], $limit, $page, base_url("/top-airing-anime?page=")) ?>
                    </section>
                </div>
            </div>
            <?php require_once(_DIR . '/require/foot.php'); ?>
        </div>
    </div>
    <?php require_once(_DIR . '/require/tempJs.php'); ?>
</body>

</html>