<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$page = ($_GET['page'] ? (int)$_GET['page'] : 1);
$avd = [
    "a" => BBcode(["pageType" => "OVA"])['title'],
    "b" => BBcode(["pageType" => "OVA"])['description'],
    "c" => BBcode(["pageType" => "OVA"])['keywords'],
    "d" => URL_LOAD
];
$limit = 32;
$paging = page_checker('movie', "WHERE public >= 1 AND type = 'OVA'", $limit, $page);
$movieList = $paging['total'] >= 1 ? showMovie("WHERE public >= 1 AND type = 'OVA' ORDER BY time DESC LIMIT {$paging['start']},$limit") : '';
?>
<!DOCTYPE html>
<html data-aid="iptfilm">

<head>
    <?php require_once(_DIR . '/require/head.php'); ?>
</head>

<body>
    <div class="container">
        <div id="wrapper">
            <?php require_once(_DIR . '/require/header.php'); ?>
            <div id="body">
                <div class="list-page full-page">
                    <section>
                        <div class="head">
                            <h1 class="title"><?= web_name() ?> LIST OVA MOVIES SERIES TV ONLINE FREE WITH DUB AND SUB LATEST UPDATE</h1>
                        </div>
                        <div class="ani items">
                            <?= $movieList ?>
                        </div>
                        <?= movie_navition($paging['total'], $limit, $page, base_url("/ova?page=")) ?>
                    </section>
                </div>
            </div>
            <?php require_once(_DIR . '/require/foot.php'); ?>
        </div>
    </div>
    <?php require_once(_DIR . '/require/tempJs.php'); ?>
</body>