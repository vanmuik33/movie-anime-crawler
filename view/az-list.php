<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$page = ($_GET['page'] ? (int)$_GET['page'] : 1);
$name = ($st[0] ? sql_escape($st[0]) : die(header("Location:" . base_url())));
$filter_name = strtolower($name);
$avd = [
    "a" => BBcode(["pageType" => "AZ List"])['title'],
    "b" => BBcode(["pageType" => "AZ List"])['description'],
    "c" => BBcode(["pageType" => "AZ List"])['keywords'],
    "d" => URL_LOAD
];
$limit = 24;
$paging = page_checker('movie', "WHERE public >= 1 AND name LIKE '$name%' OR name LIKE '$filter_name%'", $limit, $page);
$movieList = $paging['total'] >= 1 ? showMovie("WHERE public >= 1 AND name LIKE '$name%' OR name LIKE '$filter_name%' ORDER BY created DESC LIMIT {$paging['start']},$limit") : '';
?>
<!DOCTYPE html>
<html data-aid="iptfilm">

<head>
    <?php require_once(_DIR . '/require/head.php'); ?>
</head>

<body>
<?php require_once(_DIR . '/require/header.php'); ?>
    <div class="container">
        <div id="wrapper">
            <div id="body">
                <div class="list-page full-page">
                    <section>
                        <div class="head">
                            <h1 class="title"><?= $name ?> MOVIES - <?= web_name() ?></h1>
                        </div>
                        <div class="ani items">
                            <?= $movieList ?>
                        </div>
                        <?= movie_navition($paging['total'], $limit, $page, base_url("/az-list/$name?page=")) ?>
                    </section>
                </div>
            </div>
            <?php require_once(_DIR . '/require/foot.php'); ?>
        </div>
    </div>
    <?php require_once(_DIR . '/require/tempJs.php'); ?>
</body>