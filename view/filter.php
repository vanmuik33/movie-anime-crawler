<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$orders = filter();
$page = ($_GET['page'] ? (int)$_GET['page'] : 1);
$avd = [
    "a" => BBcode(["pageType" => "Lọc phim"])['title'],
    "b" => BBcode(["pageType" => "Lọc phim"])['description'],
    "c" => BBcode(["pageType" => "Lọc phim"])['keywords'],
    "d" => URL_LOAD
];
$limit = 32;
$paging = page_checker('movie', "WHERE public >= 1 $orders", $limit, $page);
$movieList = $paging['total'] >= 1 ? showMovie("WHERE public >= 1 $orders LIMIT {$paging['start']},$limit") : '';

?>
<!DOCTYPE html>
<html data-aid="iptfilm">

<head>
    <?php require_once(_DIR . '/require/head.php'); ?>
</head>

<body>
<?php require_once(_DIR . '/require/header.php'); ?>
    <div class="container">
        <div id="wrapper">
            <div id="body">
                <div class="list-page full-page">
                    <section>
                        <div class="head">
                            <h1 class="title">FILTER ANIME AT <?= web_name() ?> MOVIE FREE</h1>
                        </div>
                        <?php require_once(_DIR . '/require/filter-form.php') ?>
                        <div class="ani items">
                            <?= $movieList ?>
                        </div>
                        <?= movie_navition($paging['total'], $limit, $page, base_url("/filter" . filter_pages())) ?>
                    </section>
                </div>
            </div>
            <?php require_once(_DIR . '/require/foot.php'); ?>
        </div>
    </div>
    <?php require_once(_DIR . '/require/tempJs.php'); ?>
</body>