<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$movieId = $_GET['id'];
$info = GetDataArr('movie', "id = $movieId");
?>
<!DOCTYPE html>

<html lang="vi">
<!--begin::Head-->

<head>
    <?php require_once(_DIR . "/defult/head.php"); ?>
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true" class="app-default">
    <!--begin::Theme mode setup on page load-->
    <script>
        var defaultThemeMode = "light";
        var themeMode;
        if (document.documentElement) {
            if (document.documentElement.hasAttribute("data-theme-mode")) {
                themeMode = document.documentElement.getAttribute("data-theme-mode");
            } else {
                if (localStorage.getItem("data-theme") !== null) {
                    themeMode = localStorage.getItem("data-theme");
                } else {
                    themeMode = defaultThemeMode;
                }
            }
            if (themeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            }
            document.documentElement.setAttribute("data-theme", themeMode);
        }
    </script>
    <!--end::Theme mode setup on page load-->

    <!--begin::App-->
    <div class="d-flex flex-column flex-root app-root" id="kt_app_root">
        <!--begin::Page-->
        <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
            <!--begin::Header-->
            <?php require_once(_DIR . "/defult/header.php"); ?>
            <!--end::Header-->
            <!--begin::Wrapper-->
            <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
                <!--begin::Sidebar-->
                <?php require_once(_DIR . "/defult/sidebar.php"); ?>
                <!--end::Sidebar-->
                <!--begin::Main-->
                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                    <!--begin::Content wrapper-->
                    <div class="d-flex flex-column flex-column-fluid">
                        <!--begin::Content-->
                        <div id="kt_app_content" class="app-content flex-column-fluid">
                            <!--begin::Content container-->
                            <div id="kt_app_content_container" class="app-container container-fluid mt-6">
                                <div class="col-lg-12 mb-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Chỉnh sửa phim : <?= $info['name'] ?></h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="text-center mb-4">
                                                <?php if ($info['type'] == 'series') { ?>
                                                    <a class="btn btn-primary" href="<?= base_admin('/episode-manager?id=' . $movieId) ?>">Quản lý episode</a>
                                                <?php } else { ?>
                                                    <a href="<?= base_admin("/episode-list?movieId={$info['id']}") ?>" class="btn btn-primary btn-sm">Quản lý episode</a>
                                                <?php } ?>
                                            </div>
                                            <form submit-ajax="ngockush" method="post" table="movie" action="update-database" id-table="<?= $movieId ?>" class="fw-bold fs-5 row">
                                                <div class="form-group col-6 mb-2">
                                                    <label>Tên phim</label>
                                                    <input type="text" name="name" class="form-control" value="<?= $info['name'] ?>">
                                                </div>
                                                <div class="form-group col-6 mb-2">
                                                    <label>Tên khác</label>
                                                    <input type="text" name="origin_name" class="form-control" value="<?= $info['origin_name'] ?>">
                                                </div>
                                                <div class="form-group col-lg-6 mb-2 relative">
                                                    <label>Link ảnh thumbnail</label>
                                                    <input type="text" class="form-control" name="thumb" value="<?= $info['thumb'] ?>">
                                                    <button type="button" class="btn-upload" data-input-id="#thumb">
                                                        <i class="fa-solid fa-cloud-arrow-up text-danger" style="font-size: 25px;"></i>
                                                    </button>
                                                    <input type="file" id="thumb" data-result="thumb" style="display: none;" accept="image/*">
                                                </div>
                                                <div class="form-group col-lg-6 mb-2 relative">
                                                    <label>Link ảnh Cover</label>
                                                    <input type="text" class="form-control" name="coverUrl" value="<?= $info['coverUrl'] ?>">
                                                    <button type="button" class="btn-upload" data-input-id="#coverUrl">
                                                        <i class="fa-solid fa-cloud-arrow-up text-danger" style="font-size: 25px;"></i>
                                                    </button>
                                                    <input type="file" id="coverUrl" data-result="coverUrl" style="display: none;" accept="image/*">
                                                </div>
                                                <div class="form-group col-lg-6 mb-2">
                                                    <label>Chọn thể loại</label>
                                                    <select class="form-select form-select-solid" name="genres[]" data-control="select2" data-close-on-select="false" data-placeholder="Vui lòng chọn thể loại của phim" data-allow-clear="true" multiple="multiple">
                                                        <?php
                                                        $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "genres ORDER BY id DESC");
                                                        while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
                                                            echo '<option value="' . $row['slug'] . '" ' . arr_selected($info['genres'], $row['slug']) . '>' . $row['name'] . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>

                                                <div class="form-group col-lg-6 mb-2">
                                                    <label>Chọn quốc gia</label>
                                                    <select class="form-select" name="country">
                                                        <?php
                                                        $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "country ORDER BY id DESC");
                                                        while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
                                                            echo '<option value="' . $row['slug'] . '" ' . selected($info['country'], $row['slug']) . '>' . $row['name'] . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>

                                                <div class="form-group col-lg-3 mb-2">
                                                    <label>Năm phim</label>
                                                    <input type="number" name="year" class="form-control" value="<?= $info['year'] ?>">
                                                </div>
                                                <div class="form-group col-lg-3 mb-2">
                                                    <label>Trạng thái tập</label>
                                                    <input type="text" name="ep_status" class="form-control" value="<?= $info['ep_status'] ?>">
                                                </div>
                                                <div class="form-group col-lg-3 mb-2">
                                                    <label>Thời lượng</label>
                                                    <input type="text" name="duration" class="form-control" value="<?= $info['duration'] ?>">
                                                </div>

                                                <div class="form-group col-lg-3 mb-2">
                                                    <label>Loại phim</label>
                                                    <select class="form-select" name="type">
                                                        <option value="Movie" <?= selected($info['type'], 'Movie') ?>>Phim lẻ</option>
                                                        <option value="Series" <?= selected($info['type'], 'Series') ?>>Phim bộ</option>
                                                        <option value="Specials" <?= selected($info['type'], 'Specials') ?>>Specials</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-4 mb-2">
                                                    <label>Trạng thái</label>
                                                    <select class="form-select" name="status">
                                                        <option value="ongoing" <?= selected($info['status'], 'ongoing') ?>>Đang chiếu</option>
                                                        <option value="completed" <?= selected($info['status'], 'completed') ?>>Hoàn thành</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-4 mb-2">
                                                    <label>Hiển thị trên slider</label>
                                                    <select class="form-select" name="onSlider">
                                                        <option value="1" <?= selected($info['onSlider'], '1') ?>>Có</option>
                                                        <option value="0" <?= selected($info['onSlider'], '0') ?>>Không</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-4 mb-2">
                                                    <label>Công khai</label>
                                                    <select class="form-select" name="public">
                                                        <option value="1" <?= selected($info['public'], '1') ?>>Công khai</option>
                                                        <option value="0" <?= selected($info['public'], '0') ?>>Riêng tư</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-12 mb-2">
                                                    <label>Nội dung phim</label>
                                                    <textarea type="text" class="form-control" name="content"><?= $info['content'] ?></textarea>
                                                </div>
                                                <div class="form-group mb-2">
                                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Content container-->
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Content wrapper-->
                    <!--begin::Footer-->
                    <?php require_once(_DIR . "/defult/footer.php"); ?>
                    <!--end::Footer-->
                </div>
                <!--end:::Main-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::App-->

    <?php require_once(_DIR . "/defult/js.php"); ?>
    <script>
        new Tagify(document.querySelector(".kt_tagify"));
        new Tagify(document.querySelector(".kt_tagify2"));
        tinymce.init({
            selector: 'textarea#content',
            setup: function(editor) {
                editor.on('change', function() {
                    editor.save(); // Lưu nội dung vào textarea ẩn
                });
            },
            plugins: [
                'a11ychecker', 'advlist', 'advcode', 'advtable', 'autolink', 'checklist', 'export',
                'lists', 'link', 'image', 'charmap', 'preview', 'anchor', 'searchreplace', 'visualblocks',
                'powerpaste', 'fullscreen', 'formatpainter', 'insertdatetime', 'media', 'table', 'help', 'wordcount'
            ],
            toolbar: 'undo redo | a11ycheck casechange blocks | bold italic backcolor | alignleft aligncenter alignright alignjustify |' +
                'bullist numlist checklist outdent indent | removeformat | code table help'
        });
    </script>
</body>
<!--end::Body-->

</html>