<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
header('content-type:application/json');
if (!$_SESSION['auth']) die(MessageJson(false, "Bạn chưa đăng nhập"));
$action = ($_POST['action'] ? $_POST['action'] : $_GET['action']);
$username = sql_escape($_SESSION['admin']);
if ($action == 'table_detele') {
    $table = sql_escape($_POST['table']);
    $id = sql_escape($_POST['id']);
    $mysql->delete($table, "", "id = '$id'");
    if ($table == 'movie') {
        $mysql->delete('episode', '', "movieId = $id");
    }
    die(MessageJson(true, "Xóa Thành Công 1 Trường"));
} else if ($action == 'insert-database') {
    $table = $_GET['table'];
    $check = 0;
    foreach ($_POST as $key => $value) {
        $check++;
        $colums .= "$key" . ($check < count($_POST) ? "," : "");
        $query .= "'" . value_detect($key, $value, $table) . "'" . ($check < count($_POST) ? "," : "");
    }
    $mysql->insert($table, $colums, $query);
    die(MessageJson(true, "Thêm thành công"));
} else if ($action == 'update-database') {
    $table = $_GET['table'];
    $table_id = $_GET['table_id'];
    $check = 0;
    foreach ($_POST as $key => $value) {
        $check++;
        $query .= "$key = '" . value_detect($key, $value, $table) . "'" . ($check < count($_POST) ? "," : "");
    }
    $mysql->update($table, $query, "id = '$table_id'");
    die(MessageJson(true, "Cập Nhật thành công"));
} else if ($action == 'cleaner_table') {
    $table = sql_escape($_POST['table']);
    $mysql->delete_all($table);
    die(MessageJson(true, "Dọn Dẹp Thành Công"));
} else if ($action == 'upload-images') {
    if (isset($_FILES['image'])) {
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_tmp = $_FILES['image']['tmp_name'];
        $file_type = $_FILES['image']['type'];
        $expensions = array("jpeg", "jpg", "png", "gif", "ico");
        $file_name_new = chuyenslug(explode(".", $file_name)[0]);
        $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
        if (in_array($file_ext, $expensions) === false) die(MessageJson(false, "Định Dạng File Không Hợp Lệ "));
        if ($file_size > 2097152) die(MessageJson(false, "Dung Lượng File Vượt Quá Dung Lượng Cho Phép"));
        $year = date('Y');
        $month = date('m');
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . UPLOAD_DIR . "/$year/$month")) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . UPLOAD_DIR . "/$year/$month", 0777, true);
        }
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . UPLOAD_DIR . "/$year/$month/$file_name_new.$file_ext")) {
            die(MessageJson(true, HOME . "/$year/$month/$file_name_new.$file_ext"));
        } else {
            if (resize_crop_image($file_tmp, $_SERVER["DOCUMENT_ROOT"] . UPLOAD_DIR . "/$year/$month/$file_name_new.$file_ext", 80)) {
                die(MessageJson(true, HOME . UPLOAD_DIR . "/$year/$month/$file_name_new.$file_ext"));
            } else {
                die(MessageJson(false, "Upload hình ảnh không thành công"));
            }
        }
    } else die(MessageJson(false, "Vui Lòng Chọn File"));
} else if ($action == 'delete-checkbox') {
    $table = $_POST['table'];
    $myCheckboxes = $_POST['myCheckboxes'];
    $success = 0;
    foreach ($myCheckboxes as $value) {
        $mysql->delete($table, '', "id = $value");
        if ($table == 'movie') {
            $mysql->delete('episode', '', "movieId = $value");
        }
        $success++;
    }
    die(MessageJson(true, "Xóa thành công $success trường"));
} else if ($action == 'clear-cache-site') {
    $phpfastcache->clear();
    die(MessageJson(true, "Xóa cache website thành công"));
} else if ($action == 'removeObject') {
    $key = (int)$_POST['key'];
    $object_data = jsonDecode($_POST['object_data']);
    if ($object_data['action'] == 'removeSeason') {
        $movieId = (int)$object_data['movieId'];
        $seasonList = jsonDecode(get_data_multi('data', 'episode', "movieId = $movieId"));
        unset($seasonList[$key]);
        $seasonList = array_values($seasonList);
        $seasonList = jsonEncode($seasonList);
        $mysql->update('episode', "data = '$seasonList'", "movieId = $movieId");
        die(MessageJson(true, "Xoá season thành công"));
    } else if ($object_data['action'] == 'removeEpisode') {
        $movieId = (int)$object_data['movieId'];
        $season = (int)$object_data['season'];
        $episode = (int)$object_data['episode'];

        $seasonList = jsonDecode(get_data_multi('data', 'episode', "movieId = $movieId"));
        unset($seasonList[$season]['season_episode'][$key]);
        $seasonList = array_values($seasonList);
        $seasonList = jsonEncode($seasonList);
        $mysql->update('episode', "data = '$seasonList'", "movieId = $movieId");
        die(MessageJson(true, "Xoá episode thành công"));
    } else if ($object_data['action'] == 'removeServer') {
        $movieId = (int)$object_data['movieId'];
        $season = (int)$object_data['season'];
        $episode = (int)$object_data['episode'];

        $seasonList = jsonDecode(get_data_multi('data', 'episode', "movieId = $movieId"));
        unset($seasonList[$season]['season_episode'][$episode]['episode_server'][$key]);
        $seasonList = array_values($seasonList);
        $seasonList = jsonEncode($seasonList);
        $mysql->update('episode', "data = '$seasonList'", "movieId = $movieId");
        die(MessageJson(true, "Xoá server thành công"));
    }
} else if ($action == 'removeServer') {
    $episodeId = (int)$_POST['episodeId'];
    $remove_key = (int)$_POST['remove_key'];

    $episode_data = jsonDecode(get_data_multi('data', 'episode', "id = $episodeId"));
    unset($episode_data[($remove_key - 1)]);
    $episode_data = array_values($episode_data);
    $episode_data = jsonEncode($episode_data);
    $mysql->update('episode', "data = '$episode_data'", "id = $episodeId");
    die(MessageJson(true, "Xoá server thành công"));
} else if ($action == 'update-episode') {
    $episode_id = $_POST['episode_id'];
    $episode_name = $_POST['episode_name'];
    $episode_num = $_POST['episode_num'];
    foreach ($episode_id as $k => $episodeId) {
        $episodeName = $episode_name[$k];
        $episodeNum = $episode_num[$k];
        $mysql->update("episode", "num = $episodeNum, name = '$episodeName'", "id = $episodeId");
    }
    die(MessageJson(true, "Cập nhật episode thành công"));
} else if ($action == 'update-server-episode') {
    $episodeId = (int)$_POST['episodeId'];

    $server_key = $_POST['server_key'];
    $server_name = $_POST['server_name'];
    $server_link = $_POST['server_link'];
    $server_type = $_POST['server_type'];

    $episode_data = jsonDecode(get_data_multi('data', 'episode', "id = $episodeId"));
    foreach ($server_key as $k => $v) {
        $serverName = $server_name[$k];
        $serverLink = $server_link[$k];
        $serverType = $server_type[$k];
        $episode_data[($v - 1)]['server_name'] = $serverName;
        $episode_data[($v - 1)]['server_link'] = $serverLink;
        $episode_data[($v - 1)]['server_type'] = $serverType;
    }
    $episode_data = array_values($episode_data);
    $episode_data = jsonEncode($episode_data);
    $mysql->update('episode', "data = '$episode_data'", "id = $episodeId");
    die(MessageJson(true, "Cập nhật server thành công"));
} else if ($action == 'loadPageType') {
    $pageType = $_POST['pageType'];
    $page = get_info('advanced', "pageType = '$pageType'");
    die(MessageJson(true, $page));
} else if ($action == 'update-advanced') {
    $pageType = $_POST['pageType'];
    $title = $_POST['title'];
    $description = $_POST['description'];
    $keywords = $_POST['keywords'];
    $mysql->update('advanced', "title = '$title',description = '$description',keyword = '$keywords'", "pageType = '$pageType'");
    die(MessageJson(true, "Cập nhật trang $pageType thành công"));
} else if ($action == 'add_news_season') {
    $movieId = (int)$_POST['movieId'];
    $seasonNum = (int)$_POST['seasonNum'];
    if ($seasonNum <= 0) die(MessageJson(false, "Vui lòng nhập số season cần add"));

    $season_num = (int)get_data_multi('num', "season", "movieId = $movieId ORDER BY num DESC");
    for ($i = 0; $i < $seasonNum; $i++) {
        $season_num++;
        $mysql->insert('season', 'num,movieId,name', "$season_num,$movieId,'Season $season_num'");
    }
    die(MessageJson(true, "Thêm thành công $seasonNum mùa mới"));
} else if ($action == 'add-episode') {
    $movieId = (int)$_POST['movieId'];

    $episode_name = $_POST['episode_name'];

    $server_name = $_POST['server_name'];
    $server_link = $_POST['server_link'];
    $server_type = $_POST['server_type'];

    $server_data = [];
    $server_data[] = array(
        "server_name" => $server_name,
        "server_link" => $server_link,
        "server_type" => $server_type
    );
    $server_data = jsonEncode($server_data);
    $episode_num = get_data_multi('num', 'episode', "movieId = $movieId ORDER BY num DESC") ?? 0;
    $episode_num++;
    $mysql->insert('episode', 'movieId,num,name,data', "$movieId,$episode_num,'$episode_name','$server_data'");
    $timeUpdate = date('Y-m-d H:i:s');
    $mysql->update('movie', "created = '$timeUpdate'", "id = $movieId");
    die(MessageJson(true, "Thêm thành công episode : $episode_name"));
} else if ($action == 'add-server-news') {
    $episodeId = (int)$_POST['episodeId'];

    $server_name = $_POST['server_name'];
    $server_link = $_POST['server_link'];
    $server_type = $_POST['server_type'];

    $server_data = jsonDecode(get_data_multi('data', 'episode', "id = $episodeId"));
    $server_data[] = array(
        "server_name" => $server_name,
        "server_link" => $server_link,
        "server_type" => $server_type
    );
    $server_data = jsonEncode($server_data);
    $mysql->update('episode', "data = '$server_data'", "id = $episodeId");
    die(MessageJson(true, "Thêm thành công episode"));
}
