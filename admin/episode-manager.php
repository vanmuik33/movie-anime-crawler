<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$movieId = $_GET['id'];
$info = GetDataArr('movie', "id = $movieId");
?>
<!DOCTYPE html>

<html lang="vi">
<!--begin::Head-->

<head>
    <?php require_once(_DIR . "/defult/head.php"); ?>
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true" class="app-default" totalSv="0">
    <!--begin::Theme mode setup on page load-->
    <script>
        var defaultThemeMode = "light";
        var themeMode;
        if (document.documentElement) {
            if (document.documentElement.hasAttribute("data-theme-mode")) {
                themeMode = document.documentElement.getAttribute("data-theme-mode");
            } else {
                if (localStorage.getItem("data-theme") !== null) {
                    themeMode = localStorage.getItem("data-theme");
                } else {
                    themeMode = defaultThemeMode;
                }
            }
            if (themeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            }
            document.documentElement.setAttribute("data-theme", themeMode);
        }
    </script>
    <!--end::Theme mode setup on page load-->

    <!--begin::App-->
    <div class="d-flex flex-column flex-root app-root" id="kt_app_root">
        <!--begin::Page-->
        <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
            <!--begin::Header-->
            <?php require_once(_DIR . "/defult/header.php"); ?>
            <!--end::Header-->
            <!--begin::Wrapper-->
            <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
                <!--begin::Sidebar-->
                <?php require_once(_DIR . "/defult/sidebar.php"); ?>
                <!--end::Sidebar-->
                <!--begin::Main-->
                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                    <!--begin::Content wrapper-->
                    <div class="d-flex flex-column flex-column-fluid">
                        <!--begin::Content-->
                        <div id="kt_app_content" class="app-content flex-column-fluid">
                            <!--begin::Content container-->
                            <div id="kt_app_content_container" class="app-container container-fluid mt-6">
                                <div class="col-lg-12 mb-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Quản lý season và episode phim : <?= $info['name'] ?></h4>
                                        </div>
                                        <div class="card-body">
                                            <?php if ($info['type'] == 'series') { ?>
                                                <div class="mb-4">
                                                    <input type="number" class="form-control" name="season_news" data-movie="<?= $movieId ?>" placeholder="Nhập số season cần add và enter" required>
                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table table-row-bordered gy-5">
                                                        <thead>
                                                            <tr class="fw-semibold fs-6 text-muted">
                                                                <th><input type="checkbox" name="checkAll" id="checkAll" /></th>
                                                                <th>Tên season</th>
                                                                <th>Số tập phim</th>
                                                                <th>Hành Động</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="fw-bold fs-6">
                                                            <?php
                                                            $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "season WHERE movieId = {$info['id']} ORDER BY id DESC");
                                                            while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
                                                            ?>
                                                                <tr id="season_<?= $row['id'] ?>">
                                                                    <td><input type="checkbox" class="delete_id" value="<?= $row['id'] ?>" /></td>
                                                                    <td><?= $row['name'] ?></td>
                                                                    <td><?= get_total('episode', "WHERE seasonId = {$row['id']}") ?></td>
                                                                    <td class="btn-group">
                                                                        <a href="<?= base_admin("/episode-list?movieId={$info['id']}&seasonId={$row['id']}") ?>" class="btn btn-primary btn-sm">Episode</a>
                                                                        <button type="button" class="btn btn-danger btn-sm" onclick="table_detele('season', <?= $row['id'] ?>)"><i class="fa-solid fa-circle-xmark"></i></button>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Content container-->
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Content wrapper-->
                    <!--begin::Footer-->
                    <?php require_once(_DIR . "/defult/footer.php"); ?>
                    <!--end::Footer-->
                </div>
                <!--end:::Main-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::App-->

    <?php require_once(_DIR . "/defult/js.php"); ?>

</body>
<!--end::Body-->

</html>