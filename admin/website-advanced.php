<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$data_table = 'advanced';
$patch = 'website-advanced';
$page = ($_GET['p'] ? (int)$_GET['p'] : 1);
$keyword = ($_GET['keyword'] ? $_GET['keyword'] : '');
?>
<!DOCTYPE html>

<html lang="vi">
<!--begin::Head-->

<head>
    <?php require_once(_DIR . "/defult/head.php"); ?>
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true" class="app-default">
    <!--begin::Theme mode setup on page load-->
    <script>
        var defaultThemeMode = "light";
        var themeMode;
        if (document.documentElement) {
            if (document.documentElement.hasAttribute("data-theme-mode")) {
                themeMode = document.documentElement.getAttribute("data-theme-mode");
            } else {
                if (localStorage.getItem("data-theme") !== null) {
                    themeMode = localStorage.getItem("data-theme");
                } else {
                    themeMode = defaultThemeMode;
                }
            }
            if (themeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            }
            document.documentElement.setAttribute("data-theme", themeMode);
        }
    </script>
    <!--end::Theme mode setup on page load-->

    <!--begin::App-->
    <div class="d-flex flex-column flex-root app-root" id="kt_app_root">
        <!--begin::Page-->
        <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
            <!--begin::Header-->
            <?php require_once(_DIR . "/defult/header.php"); ?>
            <!--end::Header-->
            <!--begin::Wrapper-->
            <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
                <!--begin::Sidebar-->
                <?php require_once(_DIR . "/defult/sidebar.php"); ?>
                <!--end::Sidebar-->
                <!--begin::Main-->
                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                    <!--begin::Content wrapper-->
                    <div class="d-flex flex-column flex-column-fluid">
                        <!--begin::Content-->
                        <div id="kt_app_content" class="app-content flex-column-fluid">
                            <!--begin::Content container-->
                            <div id="kt_app_content_container" class="app-container container-fluid mt-6">
                                <div class="col-lg-12 mb-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Cài đặt nâng cao</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="form-group col-12 mb-4">
                                                <select class="form-control select_page">
                                                    <option>---Chọn trang cần sửa---</option>
                                                    <?php
                                                    $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "advanced  ORDER BY pageType DESC");
                                                    while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
                                                    ?>
                                                        <option value="<?= $row['pageType'] ?>">Trang <?= $row['pageType'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="alert alert-info">
                                                <h4>BBcode hỗ trợ </h4>
                                                <p><span class="badge bg-primary">{web_title}</span>: Tiêu đề mặc định</p>
                                                <p><span class="badge bg-primary">{web_desc}</span>: Mô tả mặc định</p>
                                                <p><span class="badge bg-primary">{web_keywords}</span>: Từ khoá mặc định</p>
                                                <p><span class="badge bg-primary">{web_year}</span>: Năm hiện tại</p>
                                                <p><span class="badge bg-primary">{web_name}</span>: Tên của trang web</p>
                                                <div id="bbcode"></div>
                                            </div>
                                            <form submit-ajax="ngockush" style="display: none;" method="post" action="update-advanced" class="fw-bold fs-5 row">
                                                <input type="text" style="display: none;" name="pageType">
                                                <div class="form-group col-12 mb-4">
                                                    <label>Tiêu đề</label>
                                                    <input type="text" name="title" class="form-control">
                                                </div>
                                                <div class="form-group col-12 mb-4">
                                                    <label>Mô tả</label>
                                                    <input type="text" name="description" class="form-control">
                                                </div>
                                                <div class="form-group col-12 mb-4">
                                                    <label>Từ khóa</label>
                                                    <input type="text" name="keywords" class="form-control">
                                                </div>
                                                <div class="form-group mb-2">
                                                    <button type="submit" class="btn btn-primary">Cập nhật cài đặt</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Content container-->
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Content wrapper-->
                    <!--begin::Footer-->
                    <?php require_once(_DIR . "/defult/footer.php"); ?>
                    <!--end::Footer-->
                </div>
                <!--end:::Main-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::App-->
    <?php require_once(_DIR . "/defult/js.php"); ?>
</body>
<!--end::Body-->

</html>