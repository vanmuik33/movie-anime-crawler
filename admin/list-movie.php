<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$data_table = 'movie';
$patch = 'list-movie';
$page = ($_GET['p'] ? (int)$_GET['p'] : 1);
$keyword = ($_GET['keyword'] ? $_GET['keyword'] : '');
?>
<!DOCTYPE html>

<html lang="vi">
<!--begin::Head-->

<head>
    <?php require_once(_DIR . "/defult/head.php"); ?>
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true" class="app-default">
    <!--begin::Theme mode setup on page load-->
    <script>
        var defaultThemeMode = "light";
        var themeMode;
        if (document.documentElement) {
            if (document.documentElement.hasAttribute("data-theme-mode")) {
                themeMode = document.documentElement.getAttribute("data-theme-mode");
            } else {
                if (localStorage.getItem("data-theme") !== null) {
                    themeMode = localStorage.getItem("data-theme");
                } else {
                    themeMode = defaultThemeMode;
                }
            }
            if (themeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            }
            document.documentElement.setAttribute("data-theme", themeMode);
        }
    </script>
    <!--end::Theme mode setup on page load-->

    <!--begin::App-->
    <div class="d-flex flex-column flex-root app-root" id="kt_app_root">
        <!--begin::Page-->
        <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
            <!--begin::Header-->
            <?php require_once(_DIR . "/defult/header.php"); ?>
            <!--end::Header-->
            <!--begin::Wrapper-->
            <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
                <!--begin::Sidebar-->
                <?php require_once(_DIR . "/defult/sidebar.php"); ?>
                <!--end::Sidebar-->
                <!--begin::Main-->
                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                    <!--begin::Content wrapper-->
                    <div class="d-flex flex-column flex-column-fluid">
                        <!--begin::Content-->
                        <div id="kt_app_content" class="app-content flex-column-fluid">
                            <!--begin::Content container-->
                            <div id="kt_app_content_container" class="app-container container-fluid mt-6">

                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Danh sách phim</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="mb-2">
                                                <button type="button" class="btn btn-danger btn-sm" onclick="cleaner_table('<?= $data_table ?>')">Xóa Toàn Bộ</button>
                                                <button type="button" class="btn btn-danger btn-sm" id="detele-selected" data-table="<?= $data_table ?>">Xóa đã chọn</button>
                                                <button type="button" class="btn btn-primary btn-sm toggle-show-body" data-show=".search-body">Tìm kiếm <i class="fa fa-plus"></i></button>
                                            </div>
                                            <div class="search-body mt-4" style="display: none;">
                                                <form action="<?= base_admin("/$patch") ?>" method="get">
                                                    <?php if ($page) { ?>
                                                        <input type="text" style="display: none;" name="page" value="<?= $page ?>">
                                                    <?php } ?>
                                                    <div class="form-group">
                                                        <label for="">Nhập từ khóa bạn cần tìm</label>
                                                        <input type="text" name="keyword" class="form-control" placeholder="VD : abcxyz">
                                                    </div>
                                                    <div class="form-group mt-2 text-center">
                                                        <button type="submit" class="btn btn-sm btn-primary">Tìm kiếm</button>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-row-bordered gy-5">
                                                    <thead>
                                                        <tr class="fw-semibold fs-6 text-muted">
                                                            <th><input type="checkbox" name="checkAll" id="checkAll" /></th>
                                                            <th>Tên phim</th>
                                                            <th>Chất lượng</th>
                                                            <th>Năm phim</th>
                                                            <th>Loại phim</th>
                                                            <th>Trạng thái</th>
                                                            <th>Lượt xem</th>
                                                            <th>Lượt vote</th>
                                                            <th>Công khai</th>
                                                            <th>Hành Động</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="fw-bold fs-6">
                                                        <?php
                                                        $SQL_Feidls = ($keyword ? "AND name LIKE '%$keyword%' OR origin_name  LIKE '%$keyword%'" : '');
                                                        $limit = 24;
                                                        $a = page_checker($data_table, "WHERE id >= 1 $SQL_Feidls", $limit, $page);
                                                        if ($a['total'] >= 1) {
                                                            $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "$data_table WHERE id >= 1 $SQL_Feidls ORDER BY id DESC LIMIT {$a['start']},$limit");
                                                            while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
                                                        ?>
                                                                <tr id="<?= $data_table ?>_<?= $row['id'] ?>">
                                                                    <td><input type="checkbox" class="delete_id" value="<?= $row['id'] ?>" /></td>
                                                                    <td>

                                                                        <img style="max-width: 100px;border-radius: 6px;" src="<?= $row['thumb'] ?>" alt="">
                                                                        <div style="width: 150px;overflow: auto;height: 30px;">
                                                                            <div style="display: block;"><?= $row['name'] ?></div>
                                                                        </div>
                                                                    </td>
                                                                    <td><?= $row['quality']  ?></td>
                                                                    <td><?= $row['year'] ?></td>
                                                                    <td><span class="badge bg-success"><?= $row['type'] == 'single' ? 'Phim lẻ' : 'Phim bộ' ?></span></td>
                                                                    <td><span class="badge bg-primary"><?= $row['status'] == 'ongoing' ? 'Đang cập nhật' : 'Hoàn thành' ?></span></td>
                                                                    <td><?= number_format($row['view']) ?></td>
                                                                    <td><?= number_format($row['voteNum']) ?></td>
                                                                    <td><?= $row['public'] >= 1 ? '<span class="badge bg-success">Công khai</span>' : '<span class="badge bg-warning">Riêng tư</span>' ?></td>
                                                                    <td class="btn-group">
                                                                        <a href="<?= base_admin('/edit-movie?id=' . $row['id']) ?>" class="btn btn-primary btn-sm"><i class="fa-solid fa-pen"></i></a>
                                                                        <button type="button" class="btn btn-danger btn-sm" onclick="table_detele('<?= $data_table ?>', <?= $row['id'] ?>)"><i class="fa-solid fa-circle-xmark"></i></button>
                                                                    </td>
                                                                </tr>
                                                        <?php }
                                                        } ?>
                                                    </tbody>
                                                </table>
                                                <?= view_pages($a['total'], $limit, $page, base_admin("/list-movie?keyword=$keyword&p=")) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Content container-->
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Content wrapper-->
                    <!--begin::Footer-->
                    <?php require_once(_DIR . "/defult/footer.php"); ?>
                    <!--end::Footer-->
                </div>
                <!--end:::Main-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::App-->

    <?php require_once(_DIR . "/defult/js.php"); ?>
</body>
<!--end::Body-->

</html>