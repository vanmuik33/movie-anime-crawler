<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$data_table = 'config';
$patch = 'website-setting';
$page = ($_GET['p'] ? (int)$_GET['p'] : 1);
$keyword = ($_GET['keyword'] ? $_GET['keyword'] : '');
?>
<!DOCTYPE html>

<html lang="vi">
<!--begin::Head-->

<head>
    <?php require_once(_DIR . "/defult/head.php"); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.12/ace.js"></script>
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true" class="app-default">
    <!--begin::Theme mode setup on page load-->
    <script>
        var defaultThemeMode = "light";
        var themeMode;
        if (document.documentElement) {
            if (document.documentElement.hasAttribute("data-theme-mode")) {
                themeMode = document.documentElement.getAttribute("data-theme-mode");
            } else {
                if (localStorage.getItem("data-theme") !== null) {
                    themeMode = localStorage.getItem("data-theme");
                } else {
                    themeMode = defaultThemeMode;
                }
            }
            if (themeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            }
            document.documentElement.setAttribute("data-theme", themeMode);
        }
    </script>
    <!--end::Theme mode setup on page load-->

    <!--begin::App-->
    <div class="d-flex flex-column flex-root app-root" id="kt_app_root">
        <!--begin::Page-->
        <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
            <!--begin::Header-->
            <?php require_once(_DIR . "/defult/header.php"); ?>
            <!--end::Header-->
            <!--begin::Wrapper-->
            <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
                <!--begin::Sidebar-->
                <?php require_once(_DIR . "/defult/sidebar.php"); ?>
                <!--end::Sidebar-->
                <!--begin::Main-->
                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                    <!--begin::Content wrapper-->
                    <div class="d-flex flex-column flex-column-fluid">
                        <!--begin::Content-->
                        <div id="kt_app_content" class="app-content flex-column-fluid">
                            <!--begin::Content container-->
                            <div id="kt_app_content_container" class="app-container container-fluid mt-6">
                                <div class="col-lg-12 mb-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Thêm thể loại mới</h4>
                                        </div>
                                        <div class="card-body">
                                            <form submit-ajax="ngockush" method="post" table="<?= $data_table ?>" id-table="1" action="update-database" class="fw-bold fs-5 row">
                                                <div class="form-group col-12 mb-2">
                                                    <label>Tiêu đề</label>
                                                    <input type="text" name="title" class="form-control" value="<?= $cf['title'] ?>">
                                                </div>
                                                <div class="form-group col-6 mb-2">
                                                    <label>Mô tả</label>
                                                    <input type="text" name="description" class="form-control" value="<?= $cf['description'] ?>">
                                                </div>
                                                <div class="form-group col-6 mb-2">
                                                    <label>Từ khóa</label>
                                                    <input type="text" name="keywords" class="form-control" value="<?= $cf['keywords'] ?>">
                                                </div>
                                                <div class="form-group col-lg-6 mb-2 relative">
                                                    <label>Ảnh mô tả</label>
                                                    <input type="text" class="form-control" name="image" value="<?= $cf['image'] ?>">
                                                    <button type="button" class="btn-upload" data-input-id="#image">
                                                        <i class="fa-solid fa-cloud-arrow-up text-danger" style="font-size: 25px;"></i>
                                                    </button>
                                                    <input type="file" id="image" data-result="image" style="display: none;" accept="image/*">
                                                </div>
                                                <div class="form-group col-6 mb-2">
                                                    <label>Google site veri key</label>
                                                    <input type="text" name="googleSite" class="form-control" value="<?= $cf['googleSite'] ?>">
                                                </div>
                                                <div class="form-group col-6 mb-2">
                                                    <label>Google captcha sitekey</label>
                                                    <input type="text" name="captchaKey" class="form-control" value="<?= $cf['captchaKey'] ?>">
                                                </div>
                                                <div class="form-group col-6 mb-2">
                                                    <label>Google captcha secret</label>
                                                    <input type="text" name="captchaSecret" class="form-control" value="<?= $cf['captchaSecret'] ?>">
                                                </div>
                                                <div class="form-group col-lg-6 mb-2 relative">
                                                    <label>Favico</label>
                                                    <input type="text" class="form-control" name="favico" value="<?= $cf['favico'] ?>">
                                                    <button type="button" class="btn-upload" data-input-id="#favico">
                                                        <i class="fa-solid fa-cloud-arrow-up text-danger" style="font-size: 25px;"></i>
                                                    </button>
                                                    <input type="file" id="favico" data-result="favico" style="display: none;" accept="image/*">
                                                </div>
                                                <div class="form-group col-lg-6 mb-2 relative">
                                                    <label>Logo</label>
                                                    <input type="text" class="form-control" name="logo" value="<?= $cf['logo'] ?>">
                                                    <button type="button" class="btn-upload" data-input-id="#logo">
                                                        <i class="fa-solid fa-cloud-arrow-up text-danger" style="font-size: 25px;"></i>
                                                    </button>
                                                    <input type="file" id="logo" data-result="logo" style="display: none;" accept="image/*">
                                                </div>
                                                <div class="form-group col-6 mb-2">
                                                    <label>Facebook app id</label>
                                                    <input type="text" name="fbAppId" class="form-control" value="<?= $cf['fbAppId'] ?>">
                                                </div>
                                                <div class="form-group col-3 mb-2">
                                                    <label>Link disqus</label>
                                                    <input type="text" name="disqusEmbed" class="form-control" value="<?= $cf['disqusEmbed'] ?>">
                                                </div>
                                                <div class="form-group col-3 mb-2">
                                                    <label>Số episode show ra</label>
                                                    <input type="number" name="limit_episode" class="form-control" value="<?= $cf['limit_episode'] ?>">
                                                </div>
                                                <div class="form-group col-lg-12 mb-2">
                                                    <label>Nội dung cuối trang</label>
                                                    <textarea type="text" class="form-control editor" name="foot_content"><?= un_htmlchars($cf['foot_content']) ?></textarea>
                                                </div>
                                                <div class="form-group col-lg-12 mb-2">
                                                    <label>Thông báo phía trên player</label>
                                                    <textarea type="text" class="form-control editor" name="topNotice"><?= un_htmlchars($cf['topNotice']) ?></textarea>
                                                </div>
                                                <div class="form-group col-lg-12 mb-2">
                                                    <label>Tùy chỉnh thẻ head</label>
                                                    <textarea type="text" class="form-control" rows="5" name="headCustom"><?= un_htmlchars($cf['headCustom']) ?></textarea>
                                                </div>
                                                <div class="form-group mb-2">
                                                    <button type="submit" class="btn btn-primary">Cập nhật cài đặt</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Content container-->
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Content wrapper-->
                    <!--begin::Footer-->
                    <?php require_once(_DIR . "/defult/footer.php"); ?>
                    <!--end::Footer-->
                </div>
                <!--end:::Main-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::App-->
    <?php require_once(_DIR . "/defult/js.php"); ?>

</body>
<!--end::Body-->

</html>