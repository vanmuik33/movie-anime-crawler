<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$episodeId = (int)$_GET['episodeId'];
$info = get_info('episode', "id = $episodeId");
?>
<!DOCTYPE html>

<html lang="vi">
<!--begin::Head-->

<head>
    <?php require_once(_DIR . "/defult/head.php"); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.12/ace.js"></script>
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true" class="app-default">
    <!--begin::Theme mode setup on page load-->
    <script>
        var defaultThemeMode = "light";
        var themeMode;
        if (document.documentElement) {
            if (document.documentElement.hasAttribute("data-theme-mode")) {
                themeMode = document.documentElement.getAttribute("data-theme-mode");
            } else {
                if (localStorage.getItem("data-theme") !== null) {
                    themeMode = localStorage.getItem("data-theme");
                } else {
                    themeMode = defaultThemeMode;
                }
            }
            if (themeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            }
            document.documentElement.setAttribute("data-theme", themeMode);
        }
    </script>
    <!--end::Theme mode setup on page load-->

    <!--begin::App-->
    <div class="d-flex flex-column flex-root app-root" id="kt_app_root">
        <!--begin::Page-->
        <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
            <!--begin::Header-->
            <?php require_once(_DIR . "/defult/header.php"); ?>
            <!--end::Header-->
            <!--begin::Wrapper-->
            <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
                <!--begin::Sidebar-->
                <?php require_once(_DIR . "/defult/sidebar.php"); ?>
                <!--end::Sidebar-->
                <!--begin::Main-->
                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                    <!--begin::Content wrapper-->
                    <div class="d-flex flex-column flex-column-fluid">
                        <!--begin::Content-->
                        <div id="kt_app_content" class="app-content flex-column-fluid">
                            <!--begin::Content container-->
                            <div id="kt_app_content_container" class="app-container container-fluid mt-6">
                                <div class="col-lg-12 mb-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Danh sách server : <?= $info['name'] ?></h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="mb-4 text-center">
                                                <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#add-server-episode">
                                                    Thêm server mới
                                                </button>
                                            </div>
                                            <form submit-ajax="ngockush" method="post" action="update-server-episode" class="fw-bold fs-5 row">
                                                <input type="hidden" name="episodeId" class="form-control" value="<?= $info['id'] ?>">
                                                <div class="table-responsive">
                                                    <table class="table table-row-bordered gy-5">
                                                        <thead>
                                                            <tr class="fw-semibold fs-6 text-muted">
                                                                <th>Tên server</th>
                                                                <th>Link play</th>
                                                                <th>Type</th>
                                                                <th>Hành Động</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="fw-bold fs-6">
                                                            <?php
                                                            foreach (jsonDecode($info['data']) as $k => $v) {
                                                            ?>

                                                                <tr id="server_<?= $k + 1 ?>">
                                                                    <input type="hidden" name="server_key[]" class="form-control" value="<?= $k + 1 ?>">
                                                                    <td><input type="text" name="server_name[]" class="form-control" value="<?= $v['server_name'] ?>"></td>
                                                                    <td><input type="text" name="server_link[]" class="form-control" value="<?= $v['server_link'] ?>"></td>
                                                                    <td>
                                                                        <select class="form-select" name="server_type[]">
                                                                            <option value="embed" <?= selected('embed', $v['server_type']) ?>>Embed</option>
                                                                            <option value="direct" <?= selected('direct', $v['server_type']) ?>>Direct</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="btn-group">
                                                                        <button type="button" class="btn btn-danger btn-sm remove-server" data-remove="<?= $k + 1 ?>" episode-id="<?= $info['id'] ?>"><i class="fa-solid fa-circle-xmark"></i></button>
                                                                    </td>
                                                                </tr>
                                                            <?php }  ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="form-group mt-4 text-center">
                                                    <button type="submit" class="btn btn-sm btn-primary">Cập nhật thay đổi</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Content container-->
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Content wrapper-->
                    <!--begin::Footer-->
                    <?php require_once(_DIR . "/defult/footer.php"); ?>
                    <div class="modal fade" tabindex="-1" id="add-server-episode">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title">Thêm server mới</h3>

                                    <!--begin::Close-->
                                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                                    </div>
                                    <!--end::Close-->
                                </div>

                                <div class="modal-body">
                                    <form submit-ajax="ngockush" method="post" action="add-server-news" class="fw-bold fs-5 row">
                                        <input type="text" name="episodeId" style="display: none;" value="<?= $episodeId ?>">
                                        <div class="form-group col-3 mb-2">
                                            <label>Tên server</label>
                                            <input type="text" name="server_name" class="form-control">
                                        </div>
                                        <div class="form-group col-6 mb-2">
                                            <label>Link play</label>
                                            <input type="text" name="server_link" class="form-control">
                                        </div>
                                        <div class="form-group col-lg-3 mb-2">
                                            <label>Loại play</label>
                                            <select class="form-select" name="server_type">
                                                <option value="embed">Embed</option>
                                                <option value="direct">Direct</option>
                                            </select>
                                        </div>

                                        <div class="form-group mb-2">
                                            <button type="submit" class="btn btn-primary">Thêm </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Footer-->
                </div>
                <!--end:::Main-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::App-->
    <?php require_once(_DIR . "/defult/js.php"); ?>

</body>
<!--end::Body-->

</html>