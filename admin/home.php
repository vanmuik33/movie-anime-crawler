<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
?>
<!DOCTYPE html>

<html lang="vi">
<!--begin::Head-->

<head>
    <?php require_once(_DIR . "/defult/head.php"); ?>
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true" class="app-default">
    <!--begin::Theme mode setup on page load-->
    <script>
        var defaultThemeMode = "light";
        var themeMode;
        if (document.documentElement) {
            if (document.documentElement.hasAttribute("data-theme-mode")) {
                themeMode = document.documentElement.getAttribute("data-theme-mode");
            } else {
                if (localStorage.getItem("data-theme") !== null) {
                    themeMode = localStorage.getItem("data-theme");
                } else {
                    themeMode = defaultThemeMode;
                }
            }
            if (themeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            }
            document.documentElement.setAttribute("data-theme", themeMode);
        }
    </script>
    <!--end::Theme mode setup on page load-->

    <!--begin::App-->
    <div class="d-flex flex-column flex-root app-root" id="kt_app_root">
        <!--begin::Page-->
        <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
            <!--begin::Header-->
            <?php require_once(_DIR . "/defult/header.php"); ?>
            <!--end::Header-->
            <!--begin::Wrapper-->
            <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
                <!--begin::Sidebar-->
                <?php require_once(_DIR . "/defult/sidebar.php"); ?>
                <!--end::Sidebar-->
                <!--begin::Main-->
                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                    <!--begin::Content wrapper-->
                    <div class="d-flex flex-column flex-column-fluid">
                        <!--begin::Content-->
                        <div id="kt_app_content" class="app-content flex-column-fluid">
                            <!--begin::Content container-->
                            <div id="kt_app_content_container" class="app-container container-fluid mt-6 row">
                                <div class="row">

                                    <div class="col-lg-3 mb-2">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <h4><?= number_format(get_total('movie')) ?></h4>
                                                <h4>Tổng số phim</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-2">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <h4><?= number_format(get_total('episode')) ?></h4>
                                                <h4>Tổng số episode</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-2">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <h4><?= number_format(get_total('genres')) ?></h4>
                                                <h4>Tổng thể loại</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-2">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <h4><?= number_format(get_total('country')) ?></h4>
                                                <h4>Tổng quốc gia</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-2">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <h4><?= number_format(get_total('ads')) ?></h4>
                                                <h4>Tổng quảng cáo</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-2">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <h4><?= number_format(get_total('users')) ?></h4>
                                                <h4>Tổng số thành viên</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-2">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <h4><?= number_format(get_data_multi('SUM(view)', 'movie', "view >= 1")) ?></h4>
                                                <h4>Tổng số lượt xem</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-2">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <h4><?= $_SERVER['SERVER_ADDR'] ?></h4>
                                                <h4>Ip server</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Content container-->
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Content wrapper-->
                    <!--begin::Footer-->
                    <?php require_once(_DIR . "/defult/footer.php"); ?>
                    <!--end::Footer-->
                </div>
                <!--end:::Main-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::App-->

    <?php require_once(_DIR . "/defult/js.php"); ?>
</body>
<!--end::Body-->

</html>