<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$movieId = (int)$_GET['movieId'];
$seasonId = (int)$_GET['seasonId'];
$info = GetDataArr('movie', "id = $movieId");
?>
<!DOCTYPE html>

<html lang="vi">
<!--begin::Head-->

<head>
    <?php require_once(_DIR . "/defult/head.php"); ?>
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true" class="app-default" totalSv="0">
    <!--begin::Theme mode setup on page load-->
    <script>
        var defaultThemeMode = "light";
        var themeMode;
        if (document.documentElement) {
            if (document.documentElement.hasAttribute("data-theme-mode")) {
                themeMode = document.documentElement.getAttribute("data-theme-mode");
            } else {
                if (localStorage.getItem("data-theme") !== null) {
                    themeMode = localStorage.getItem("data-theme");
                } else {
                    themeMode = defaultThemeMode;
                }
            }
            if (themeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            }
            document.documentElement.setAttribute("data-theme", themeMode);
        }
    </script>
    <!--end::Theme mode setup on page load-->

    <!--begin::App-->
    <div class="d-flex flex-column flex-root app-root" id="kt_app_root">
        <!--begin::Page-->
        <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
            <!--begin::Header-->
            <?php require_once(_DIR . "/defult/header.php"); ?>
            <!--end::Header-->
            <!--begin::Wrapper-->
            <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
                <!--begin::Sidebar-->
                <?php require_once(_DIR . "/defult/sidebar.php"); ?>
                <!--end::Sidebar-->
                <!--begin::Main-->
                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                    <!--begin::Content wrapper-->
                    <div class="d-flex flex-column flex-column-fluid">
                        <!--begin::Content-->
                        <div id="kt_app_content" class="app-content flex-column-fluid">
                            <!--begin::Content container-->
                            <div id="kt_app_content_container" class="app-container container-fluid mt-6">
                                <div class="col-lg-12 mb-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Danh sách episode phim : <?= $info['name'] ?> <?= ($seasonId >= 1 ? " Thuộc " . get_data_multi('name', 'season', "id = $seasonId") : '') ?></h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="mb-4 text-center">
                                                <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#add-episode">
                                                    Thêm episode mới
                                                </button>
                                            </div>
                                            <form submit-ajax="ngockush" method="post" action="update-episode" class="fw-bold fs-5 row">
                                                <div class="table-responsive">
                                                    <table class="table table-row-bordered gy-5">
                                                        <thead>
                                                            <tr class="fw-semibold fs-6 text-muted">
                                                                <th><input type="checkbox" name="checkAll" id="checkAll" /></th>
                                                                <th>STT</th>
                                                                <th>Tên tập</th>
                                                                <th>Số server</th>
                                                                <th>Hành Động</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="fw-bold fs-6">
                                                            <?php
                                                            $filters = ($seasonId >= 1 ? "AND seasonId = $seasonId" : '');
                                                            $limit = 24;
                                                            $a = page_checker('episode', "WHERE movieId = $movieId $filters", $limit, $page);
                                                            if ($a['total'] >= 1) {
                                                                $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "episode WHERE movieId = $movieId $filters ORDER BY id DESC LIMIT {$a['start']},$limit");
                                                                while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
                                                                    $server = jsonDecode($row['data']);
                                                            ?>
                                                                    <tr id="episode_<?= $row['id'] ?>">
                                                                        <td><input type="checkbox" class="delete_id" value="<?= $row['id'] ?>" /></td>
                                                                        <td><input type="text" name="episode_num[]" value="<?= $row['num'] ?>"></td>
                                                                        <td>
                                                                            <input type="hidden" name="episode_id[]" value="<?= $row['id'] ?>">
                                                                            <input type="text" name="episode_name[]" value="<?= $row['name'] ?>">
                                                                        </td>
                                                                        <td><?= count($server) ?></td>
                                                                        <td class="btn-group">
                                                                            <a href="<?= base_admin("/server-list?episodeId={$row['id']}") ?>" class="btn btn-primary btn-sm">Server data</a>
                                                                            <button type="button" class="btn btn-danger btn-sm" onclick="table_detele('episode', <?= $row['id'] ?>)"><i class="fa-solid fa-circle-xmark"></i></button>
                                                                        </td>
                                                                    </tr>
                                                            <?php }
                                                            } ?>
                                                        </tbody>
                                                    </table>
                                                    <?= view_pages($a['total'], $limit, $page, base_admin("/episode-list?movieId=$movieId&season=$seasonId&p=")) ?>
                                                </div>
                                                <div class="form-group mt-4 text-center">
                                                    <button type="submit" class="btn btn-sm btn-primary">Cập nhật episode</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Content container-->
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Content wrapper-->
                    <!--begin::Footer-->
                    <?php require_once(_DIR . "/defult/footer.php"); ?>
                    <div class="modal fade" tabindex="-1" id="add-episode">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title">Thêm episode mới</h3>

                                    <!--begin::Close-->
                                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                                    </div>
                                    <!--end::Close-->
                                </div>

                                <div class="modal-body">
                                    <form submit-ajax="ngockush" method="post" action="add-episode" class="fw-bold fs-5 row">
                                        <input type="text" name="movieId" style="display: none;" value="<?= $movieId ?>">
                                        <input type="text" name="seasonId" style="display: none;" value="<?= $seasonId ?>">
                                        <div class="form-group col-12 mb-2">
                                            <label>Tên episode</label>
                                            <input type="text" name="episode_name" class="form-control">
                                        </div>
                                        <div class="form-group col-3 mb-2">
                                            <label>Tên server</label>
                                            <input type="text" name="server_name" class="form-control">
                                        </div>
                                        <div class="form-group col-6 mb-2">
                                            <label>Link play</label>
                                            <input type="text" name="server_link" class="form-control">
                                        </div>
                                        <div class="form-group col-lg-3 mb-2">
                                            <label>Loại play</label>
                                            <select class="form-select" name="server_type">
                                                <option value="embed">Embed</option>
                                                <option value="direct">Direct</option>
                                            </select>
                                        </div>

                                        <div class="form-group mb-2">
                                            <button type="submit" class="btn btn-primary btn-sm">Thêm episode mới</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Footer-->
                </div>
                <!--end:::Main-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::App-->

    <?php require_once(_DIR . "/defult/js.php"); ?>

</body>
<!--end::Body-->

</html>