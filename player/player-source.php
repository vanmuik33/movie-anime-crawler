<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
header('content-type:application/json');
$movieId = (int)$_POST['movieId'];
$episodeId = $_POST['episodeId'];
$serverId = (int)$_POST['serverId'];
if (get_total('movie', "WHERE id = '$movieId'") <= 0) die(MessageJson(false, 'This movie doesn\'t exist'));
$info = GetDataArr('movie', "id = $movieId");
if (get_total('episode', "WHERE movieId = $movieId") <= 0) die(MessageJson(false, 'This episode doesn\'t exist'));
$episode = episodeJson(get_data_multi('data', 'episode', "movieId = {$info['id']}"));
$svKey = ($serverId - 1);
$epResult = episodeCheck($episode, $episodeId);
$epNum = $epResult['ep_num'];
$epData = $epResult['data'];
$svMovie = $epData['server_data'][$svKey];

if ($svMovie['server_type'] == 'embed') {
    $playerSource = ngockush_build_iframe($svMovie['server_link']);
} else $playerSource = '<div class="text-loading">Type <b style="color: #b90000;">{' . $svMovie['server_type'] . '}</b> is not yet supported</div>';

die(json_encode([
    "status" => true,
    "data" => $playerSource
]));
