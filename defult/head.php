<title>Trình Quản Lý</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="shortcut icon" href="<?= HOME ?>/metronic/media/logos/favicon.ico" />
<!--begin::Fonts-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
<!--end::Fonts-->
<!--begin::Vendor Stylesheets(used by this page)-->
<link href="<?= HOME ?>/metronic/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?= HOME ?>/metronic/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Vendor Stylesheets-->
<!--begin::Global Stylesheets Bundle(used by all pages)-->
<link href="<?= HOME ?>/metronic/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?= HOME ?>/metronic/css/style.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Global Stylesheets Bundle-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<style>
    .relative {
        position: relative;
    }

    .btn-upload {
        position: absolute;
        top: 28px;
        right: 13px;
        background: none;
        border: none;
    }
</style>
<script src="/metronic/editor/tinymce.min.js" referrerpolicy="origin"></script>