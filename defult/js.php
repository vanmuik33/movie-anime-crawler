        <!--begin::Scrolltop-->
        <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
            <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
            <span class="svg-icon">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="currentColor" />
                    <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="currentColor" />
                </svg>
            </span>
            <!--end::Svg Icon-->
        </div>
        <!--end::Scrolltop-->
        <!--begin::Javascript-->
        <script>
            var hostUrl = "<?= HOME ?>";
        </script>
        <!--begin::Global Javascript Bundle(used by all pages)-->
        <script src="<?= HOME ?>/metronic/plugins/global/plugins.bundle.js"></script>
        <script src="<?= HOME ?>/metronic/js/scripts.bundle.js"></script>
        <script src="<?= HOME ?>/metronic/js/admin-plugin.js"></script>
        <!--end::Global Javascript Bundle-->
        <!--begin::Vendors Javascript(used by this page)-->
        <script src="<?= HOME ?>/metronic/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/radar.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/map.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/geodata/worldLow.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/geodata/continentsLow.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/geodata/usaLow.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/geodata/worldTimeZonesLow.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/geodata/worldTimeZoneAreasLow.js"></script>
        <script src="<?= HOME ?>/metronic/plugins/custom/datatables/datatables.bundle.js"></script>
        <!--end::Vendors Javascript-->
        <!--begin::Custom Javascript(used by this page)-->
        <script src="<?= HOME ?>/metronic/js/widgets.bundle.js"></script>
        <script src="<?= HOME ?>/metronic/js/custom/widgets.js"></script>
        <script src="<?= HOME ?>/metronic/js/custom/apps/chat/chat.js"></script>
        <script src="<?= HOME ?>/metronic/js/custom/utilities/modals/upgrade-plan.js"></script>
        <script src="<?= HOME ?>/metronic/js/custom/utilities/modals/create-app.js"></script>
        <script src="<?= HOME ?>/metronic/js/custom/utilities/modals/new-target.js"></script>
        <script src="<?= HOME ?>/metronic/js/custom/utilities/modals/users-search.js"></script>
        <!--end::Custom Javascript-->
        <script>
            tinymce.init({
                selector: 'textarea.editor',
                setup: function(editor) {
                    editor.on('change', function() {
                        editor.save(); // Lưu nội dung vào textarea ẩn
                    });
                },
                plugins: [
                    'a11ychecker', 'advlist', 'advcode', 'advtable', 'autolink', 'checklist', 'export',
                    'lists', 'link', 'image', 'charmap', 'preview', 'anchor', 'searchreplace', 'visualblocks',
                    'powerpaste', 'fullscreen', 'formatpainter', 'insertdatetime', 'media', 'table', 'help', 'wordcount', 'code'
                ],
                toolbar: 'undo redo | a11ycheck casechange blocks | bold italic backcolor | alignleft aligncenter alignright alignjustify |' +
                    'bullist numlist checklist outdent indent | removeformat | code table help | code'
            });
        </script>
        <!--end::Javascript-->