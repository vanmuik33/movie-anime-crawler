require('dotenv').config();
const path = require('path');
const conn = require(path.normalize(__dirname + '/database'));
const func = require(path.normalize(__dirname + '/function'));
const cheerio = require('cheerio');
const main_domain = process.env.LEECH_DOMAIN;
module.exports = {
    anime_leech_movies: async function (page) {
        conn.updateData('table_options',{'value': page},{'name':'lastest_movies_crawled_page'});
        const type = 'movie';
        const response = await func.getData(process.env.LEECH_DOMAIN + `/movieshd/page/${page}`);
        const $ = cheerio.load(response);
        const movie_item = $('#dle-content > .post');
        console.log(`Tìm thấy ${movie_item.length} ${type} ở trang ${page}`);
        for (let i = movie_item.length - 1; i >= 0; i--) {
            const movie_name = $(movie_item[i]).find('h2.namt1').text();
            const movie_links = $(movie_item[i]).find('.namt > a').attr('href');
            console.log(`[${type}] crawler => ${movie_name}`);
            const info_movie = await func.movies_info(movie_links, 'movie');
            if (info_movie) {
                console.log(`[${type}] lấy thông tin phim thành công => ${movie_name}`);
                const movie = await conn.getData('table_movie', {
                    slug: info_movie.info.slug
                });
                if (movie.length <= 0) {
                    info_movie.info.ep_status = info_movie.episode.length;
                    info_movie.info.duration = '';
                    await conn.insertData(`table_movie`, info_movie.info);
                    const new_movie = await conn.getData('table_movie', {
                        slug: info_movie.info.slug
                    });
                    var movieId = new_movie[0].id;
                    console.log(`[${type}] Thêm mới phim => ${movie_name}`);
                } else {
                    var movieId = movie[0].id;
                    console.log(`[${type}] Đã tồn tại => ${movie_name}`);
                }
                const result_episode = await func.episode_movie(movieId, info_movie.episode);
                console.log(`[${type}] ${movie_name} => Cập nhật hoặc thêm mới thành công ${result_episode} episode`);
            } else {
                console.log(`[${type}] crawler => ${movie_name} Không thành công`);
            }
        }
        page--;
        // page = (page <= 0 ? 2 : page);
        if (page >= 1) module.exports.anime_leech_movies(page);
    },
    anime_leech_tv: async function (page) {
        conn.updateData('table_options',{'value': page},{'name':'lastest_series_crawled_page'});
        const type = 'tv';
        const response = await func.getData(main_domain + `/tv/page/${page}`);
        const $ = cheerio.load(response);
        const movie_item = $('#dle-content > .post');
        console.log(`Tìm thấy ${movie_item.length} ${type} ở trang ${page}`);
        for (let i = movie_item.length - 1; i >= 0; i--) {
            const movie_name = $(movie_item[i]).find('h2.namt1').text();
            const movie_links = $(movie_item[i]).find('.namt > a').attr('href');
            console.log(`[${type}] crawler => ${movie_name}`);
            var info_movie = await func.movies_info(movie_links, type);
            if (info_movie) {
                console.log(`[${type}] lấy thông tin phim thành công => ${movie_name}`);
                const movie = await conn.getData('table_movie', {
                    slug: info_movie.info.slug
                });
                if (movie.length <= 0) {
                    info_movie.info.season = $(movie_item[i]).find('.namt > .namt2 > .namy:first-child').text();
                    info_movie.info.ep_status = $(movie_item[i]).find('.namt > .namt2 > .namy + .dot + .namy').text().replace('EPS ', '');
                    info_movie.info.duration = '';
                    await conn.insertData(`table_movie`, info_movie.info);
                    const new_movie = await conn.getData('table_movie', {
                        slug: info_movie.info.slug
                    });
                    var movieId = new_movie[0].id;
                    console.log(`[${type}] Thêm mới phim => ${movie_name}`);
                } else {
                    var movieId = movie[0].id;
                    console.log(`[${type}] Đã tồn tại => ${movie_name}`);
                }
                const result_episode = await func.episode_movie(movieId, info_movie.episode);
                console.log(`[${type}] ${movie_name} => Cập nhật hoặc thêm mới thành công ${result_episode} episode`);
            } else {
                console.log(`[${type}] crawler => ${movie_name} Không thành công`);
            }
        }
        page--;
        // page = (page <= 0 ? 15 : page);
        if (page >= 1) module.exports.anime_leech_movies(page);
    },
}