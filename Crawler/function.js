require('dotenv').config();
const axios = require('axios');
const cheerio = require('cheerio');
const slugify = require('slugify');
const path = require('path');
const conn = require(path.normalize(__dirname + '/database'));
const moment = require('moment');
require('moment-timezone');
moment.tz.setDefault('Asia/Ho_Chi_Minh');
const currentDate = moment().format('Y-M-D');
const currentTime = moment().unix();
const main_domain = process.env.LEECH_DOMAIN;

module.exports = {
    cut_html: function (begin, end, data) {
        return new Promise(async (resolve, reject) => {
            try {
                var ex = data.split(begin)[1],
                    match = ex.split(end)[0];
                (match ? resolve(match) : resolve(false));
            } catch (error) {
                resolve(false);
            }
        });
    },
    getData: (url) => {
        return new Promise(async (resolve, reject) => {
            while (true) {
                try {
                    const response = await axios.get(url);
                    resolve(response.data);
                    break;
                } catch (error) {
                }
            }
        });
    },
    postData: (url, data, headers) => {
        return new Promise(async (resolve, reject) => {
            try {
                axios.request({
                    method: 'post',
                    maxBodyLength: Infinity,
                    url: url,
                    headers: headers,
                    data: data
                })
                    .then((response) => {
                        resolve(response.data);
                    })
                    .catch((error) => {
                        resolve(false);
                    });
            } catch (error) {
                resolve(false);
            }
        });
    },
    getYearFromText: function (text) {
        var year = text.match(/\b\d{4}\b/g);
        if (!year) return null;
        for (var i = 0; i < year.length; i++) {
            var nam = parseInt(year[i]); // Chuyển chuỗi số thành số nguyên
            if (!isNaN(nam)) return nam; // Trả về năm đầu tiên hợp lệ được tìm thấy
        }
        return null;
    },
    removeHTML: function (html) {
        const $ = cheerio.load(html);
        return $.text();
    },
    info_string: function (str) {
        str.replace(/[\n\r]/g, ' ');
        return module.exports.removeHTML(str.trim());
    },
    convert_to_slug: function (str) {
        str = str.replace('(', '');
        str = str.replace(')', '');
        str = str.replace(':', '');
        const slugfy = slugify(str, {
            replacement: '-',
            lower: true,
        });
        return slugfy;
    },
    movies_info: async function (movie_links, type) {
        var movie_watch_link = '';
        if (type == 'tv') {
            movie_watch_link = movie_links.replace("/tv/watch/", "/watch-movie/");
        } else {
            movie_watch_link = movie_links.replace("/movieshd/watch/", "/watch-movie/");
        }
        const movie_watch_response = await module.exports.getData(movie_watch_link);
        if (!movie_watch_response) return false;
        const $ = cheerio.load(movie_watch_response);
        const status_movie = 'Completed';
        // if (status == 'Completed') {
        //     var status_movie = 'completed';
        // } else if (status == 'Ongoing') {
        //     var status_movie = 'ongoing';
        // } else if (status == 'Upcoming') {
        //     var status_movie = 'upcoming';
        // } else {
        //     var status_movie = 'hiatus';
        // }
        const categories = $('div.features .reset:first-child li[itemprop="genre"]').text().split(", ");

        var genre = [];
        for (const items of categories) {
            const genre_name = items;
            const genre_link = module.exports.convert_to_slug(genre_name);
            const info = await conn.getData('table_genres', {
                slug: genre_link
            });
            if (info.length <= 0) {
                await conn.insertData(`table_genres`, {
                    name: genre_name,
                    slug: genre_link
                });
                console.log(`Thêm thành công thể loại ${genre_name}`);
            }
            genre.push(genre_link);
        }
        // const episode_id = $('#watch-page').data('id');
        const movie_name = $('div.pageContent .about > h1').text();
        const episode_data = await module.exports.episode_leech($);
        var movie_type = type;
        //
        // switch (movie_type) {
        //     case 'tv':
        //         movie_type = 'TV';
        //         break;
        //     case 'ova':
        //         movie_type = 'OVA';
        //         break;
        //     case 'live-action':
        //         movie_type = 'Live Action';
        //         break;
        //     case 'ona':
        //         movie_type = 'ONA';
        //         break;
        //     case 'music':
        //         movie_type = 'Music';
        //         break;
        //     case 'special':
        //         movie_type = 'Special';
        //         break;
        //     default:
        //         movie_type = 'Movie';
        //         break;
        // }
        var year = module.exports.getYearFromText($('div.features .reset:last-child > li.red + li + li').text());
        var director = $('div.features .reset:first-child > li:nth-child(6)').text();
        var countries = $('div.features .reset:first-child li[itemprop="genre"] + li + li').text().split(", ");
        var quality = $('.pageContent .about .quality').text();

        return {
            "info": {
                "name": module.exports.info_string(movie_name),
                "origin_name": '',
                "thumb": main_domain + $('.pageContent .fullMovie .poster img').attr('src'),
                "content": module.exports.info_string($('.pageContent .about .descfull .spoilerBody').text()),
                "type": module.exports.info_string(movie_type),
                "year": year,
                "quality": quality,
                "status": status_movie,
                'director': director,
                'country': JSON.stringify(countries),
                "genres": JSON.stringify(genre),
                "slug": module.exports.convert_to_slug(movie_name),
                "created": currentTime
            },
            "episode": episode_data
        }
    },
    episode_leech: async function ($) {
        const episode_list = $('#content div.bixbox.bxcl.epcheck > div.eplister > ul > li > a');
        var episode = [];
        const episode_name = 'Full';
        var episode_num = '1';
        const episode_embed = $('.tabCont > .myvideo > span:last-child > iframe').attr('src');
        episode.push({
            episode_name,
            episode_num,
            episode_embed
        });
        return episode;
    },
    episode_movie: async function (movieId, episode_data) {
        if (episode_data.length >= 1) {
            for (const episode of episode_data) {
                const episode_name = episode.episode_name;
                const episode_num = episode.episode_num;
                const episode_embed = episode.episode_embed;
                const episode_info = await conn.get_info('table_episode', `movieId = ${movieId} AND name = '${episode_name}'`);
                const episode_sv = [{
                    "server_name": "Server #1",
                    "server_link": episode_embed,
                    "server_type": "embed"
                }];
                if (episode_info.length <= 0) {
                    await conn.insertData(`table_episode`, {
                        "movieId": movieId,
                        "num": episode_num,
                        "name": episode_name,
                        "data": JSON.stringify(episode_sv)
                    });
                    conn.updateData('table_movie', {
                        "created": currentTime
                    }, {
                        "id": movieId
                    })
                } else {
                    await conn.updateData('table_episode', {
                        "data": JSON.stringify(episode_sv)
                    }, {
                        "id": episode_info[0].id
                    });
                }
            }
            return episode_data.length;
        } else {
            return 0;
        }
    }
}