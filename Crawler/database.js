require('dotenv').config();

const mysql = require('mysql2');

// Tạo pool connections MySQL
const pool = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    connectionLimit: 10 // Số lượng kết nối tối đa trong pool
});

// Hàm thực thi truy vấn
const executeQuery = (query, values) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.error('Lỗi kết nối MySQL: ', err);
                reject(err);
            } else {
                connection.query(query, values, (error, results) => {
                    connection.release(); // Trả kết nối về pool sau khi hoàn thành truy vấn
                    if (error) {
                        console.error('Lỗi khi thực thi truy vấn MySQL: ', error);
                        reject(error);
                    } else {
                        resolve(results);
                    }
                });
            }
        });
    });
};

// Hàm insert dữ liệu
exports.insertData = (tableName, data) => {
    const query = `INSERT INTO ${tableName} SET ?`;
    return executeQuery(query, data);
};

// Hàm update dữ liệu
exports.updateData = (tableName, data, condition) => {
    const query = `UPDATE ${tableName} SET ? WHERE ?`;
    return executeQuery(query, [data, condition]);
};

// Hàm lấy dữ liệu
exports.getData = (tableName, condition) => {
    const query = `SELECT * FROM ${tableName} WHERE ?`;
    return executeQuery(query, condition);
};
// Hàm lấy dữ liệu
exports.get_info = (tableName, condition) => {
    const query = `SELECT * FROM ${tableName} WHERE ${condition}`;
    return executeQuery(query);
};
// Hàm sắp xếp dữ liệu
exports.orderData = (tableName, column) => {
    const query = `SELECT * FROM ${tableName} ORDER BY ${column}`;
    return executeQuery(query);
};