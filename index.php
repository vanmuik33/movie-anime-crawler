<?php
define('NotSupportHacker', true);
error_reporting(E_ALL);
date_default_timezone_set('Asia/Ho_Chi_Minh');
mb_internal_encoding("UTF-8");
ob_start();
session_start();
ini_set('memory_limit', '512M');
ini_set('display_errors', 0);
include_once(__DIR__ . '/includes/configurations.php');
include_once(__DIR__ . '/includes/function.php');
include_once(__DIR__ . '/includes/Alltemplate.php');
include_once(__DIR__ . '/includes/movieFunction.php');
include_once(__DIR__ . '/vendor/autoload.php');

use Curl\Curl;
use Phpfastcache\Helper\Psr16Adapter;

$router = new \Bramus\Router\Router();
$defaultDriver = 'Files';
$phpfastcache = new Psr16Adapter($defaultDriver);
$curl = new Curl();
$header = getallheaders();
if ($_SESSION['auth']) {
    $username = $_SESSION['auth'];
    if (get_total('users', "WHERE username = '$username'") <= 0) {
        unset($_SESSION['auth']);
    }
    $users = get_info('users', "username = '$username'");
}
include_once(__DIR__ . '/router/routers.php');
$router->run();