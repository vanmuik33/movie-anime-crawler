<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$router->get('/', function () {
    importFile("/view/homePage");
});
$router->post('/graph-api', function () {
    importFile("/view/graph-api");
});

$router->get('/sitemap.xml', function () {
    importFile("/view/sitemap");
});
$router->get('/contact', function () {
    importFile("/view/contact");
});
$router->post('/player/source', function () {
    importFile("/player/player-source");
});
$router->get('/series', function () {
    importFile("/view/series");
});
$router->get('/tv', function () {
    importFile("/view/tv");
});
$router->get('/movies', function () {
    importFile("/view/movies");
});
$router->get('/animes', function () {
    importFile("/view/animes");
});
$router->get('/ongoing', function () {
    importFile("/view/ongoing");
});
$router->get('/az-list/{name}', function ($name) {
    importFile("/view/az-list", [$name]);
});
$router->get('/anime-complete', function () {
    importFile("/view/anime-complete");
});
$router->get('/most-popular-anime', function () {
    importFile("/view/most-popular-anime");
});
$router->get('/top-airing-anime', function () {
    importFile("/view/top-airing-anime",);
});
$router->get('/ova', function () {
    importFile("/view/ova");
});
$router->get('/ona', function () {
    importFile("/view/ona");
});
$router->get('/search/{keyword}', function ($keyword) {
    importFile("/view/Search", ["keyword" => $keyword]);
});
$router->get('/filter', function () {
    importFile("/view/filter");
});

$router->get('/genre/{slug}', function ($slug) {
    importFile("/view/Genre", [$slug]);
});
$router->get('/added', function () {
    importFile("/view/added");
});
$router->get('/film/{slug}/{episode}', function ($slug, $episode) {
    importFile("/view/movieInfo", [$slug, $episode]);
});
$router->get('/film/{slug}/{episode}', function ($slug, $episode) {
        importFile("/view/movieInfo", [$slug, $episode]);
});
$router->get('/film/{slug}', function ($slug) {
    importFile("/view/movieInfo", [$slug]);
});
$router->get('/film/{slug}', function ($slug) {
    importFile("/view/movieInfo", [$slug]);
});
$router->get('/special', function () {
    importFile("/view/special");
});
$router->get('/newest', function () {
    importFile("/view/newest");
});
$router->get('/most-watched', function () {
    importFile("/view/most-watched");
});
$router->get('/upcoming', function () {
    importFile("/view/upcoming");
});
$router->get('/admin-login', function () {
    importFile("/view/admin-login");
});
$router->get('/ajax/{files}', function ($files) {
    importFile("/ajax/$files");
});
$router->post('/ajax/{files}', function ($files) {
    importFile("/ajax/$files");
});
$router->get('/Search/SearchSuggest', function () {
    global $mysql;
    $keyword = sql_escape($_GET['keyword']);
    $kw_slug = chuyenslug($_GET['keyword']);
    $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "movie WHERE name LIKE '%$keyword%' OR origin_name LIKE '%$keyword%' OR slug LIKE '%$kw_slug%' ORDER BY id DESC");
    while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
        echo '<a class="item_search_link" href="' . base_url("/Cartoon/{$row['slug']}") . '"><em>' . $row['name'] . '</em></a>';
    }
});

// Phần routers cho admin
$router->get('/dashboard/{model}', function ($model) {
    define('NotSupportHacker', true);
    $models = ($model ? $model : 'home');
    if (!$_SESSION['auth']) header('location: /admin-login');
    importFile("/admin/$models");
});
$router->get('/dashboard', function () {
    if (!$_SESSION['auth']) header('location: /admin-login');
    importFile("/admin/home");
});
$router->post('/dashboard/graph-api', function () {
    if (!$_SESSION['auth']) header('location: /admin-login');
    importFile("/admin/graph-api");
});
