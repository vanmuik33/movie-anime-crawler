<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
?>
    <title><?= $avd['a'] ? $avd['a'] : $cf['title'] ?></title>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="120x120" href="<?= $cf['favico'] ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $cf['favico'] ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $cf['favico'] ?>">
    <link rel="mask-icon" href="<?= $cf['favico'] ?>" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="theme-color" content="#ffffff">
    <meta name='viewport' content='width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1'/>
    <link rel="shortcut icon" href="<?= $cf['favico'] ?>" type="image/x-icon"/>
    <meta name="title" content="<?= $avd['a'] ? $avd['a'] : $cf['title'] ?>"/>
    <meta name="description" content="<?= $avd['b'] ? $avd['b'] : $cf['description'] ?>"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@<?= web_name() ?>"/>
    <meta name="twitter:creator" content="@<?= web_name() ?>"/>
    <meta name="twitter:title" content="<?= $avd['a'] ? $avd['a'] : $cf['title'] ?>"/>
    <meta name="twitter:description" content="<?= $avd['b'] ? $avd['b'] : $cf['description'] ?>"/>
    <meta property="og:title" content="<?= $avd['a'] ? $avd['a'] : $cf['title'] ?>"/>
    <meta property="og:description" content="<?= $avd['b'] ? $avd['b'] : $cf['description'] ?>"/>
    <meta property="og:image" content="<?= $avd['e'] ? $avd['e'] : $cf['image'] ?>"/>
    <meta property="og:image:type" content="image/jpeg"/>
    <meta property="og:image:width" content="650"/>
    <meta property="og:image:height" content="350"/>
    <meta property="og:url" content="<?= $avd['d'] ? $avd['d'] : HOME ?>"/>
    <meta property="og:site_name" content="<?= web_name() ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:locale" content="en_US"/>
    <meta name="robots" content="noindex, nofollow"/>
    <link href="<?= $avd['d'] ? $avd['d'] : HOME ?>" rel="canonical"/>
<?php if ($cf['googleSite']) { ?>
    <meta name="google-site-verification" content="<?= $cf['googleSite'] ?>"/>
<?php } ?>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"/>
    <link rel="preconnect" href="https:////fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="//fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/Swiper/5.4.5/css/swiper.min.css">
    <link rel="stylesheet" type="text/css"
          href="//cdnjs.cloudflare.com/ajax/libs/tooltipster/4.2.8/css/tooltipster.bundle.min.css">
    <link rel="stylesheet" href="/assets/min/all.css?v=1708756258"/>
    <style>
        .website-ads {
            text-align: center;
            margin-bottom: 10px;
            display: flex;
            justify-content: center;
        }

        .website-ads img {
            width: 100%;
            height: 100%;
        }
    </style>
<?php if (isset($_GET['color1']) || isset($_GET['color2'])) : ?>
    <?php
    $color1 = $_GET['color1'];
    $color2 = $_GET['color2'];
    //check if the cookie is exist
    if (!isset($_COOKIE['color1']) && !isset($_COOKIE['color2'])) {
        //set the cookie 1h
        setcookie("color1", $color1, [
            "expires" => time() + 3600,
            "path" => "/",
            "secure" => true,
            "samesite" => "None"
        ]);
        setcookie("color2", $color2, [
            "expires" => time() + 3600,
            "path" => "/",
            "secure" => true,
            "samesite" => "None"
        ]);
        setcookie('logo', $color2, time() + 3600, '/');
    }
    ?>
<?php endif ?>
<?php
if (isset($_GET['logo'])) {
    $logo = $_GET['logo'];
    setcookie("logo", $logo, [
        "expires" => time() + 3600,
        "path" => "/",
        "secure" => true,
        "samesite" => "None"
    ]);
}
?>

    <style>
        :root {
            --primary-color: #<?= $_COOKIE['color1'] ?>;
            --secondary-color: #<?= $_COOKIE['color2'] ?>;
        }
    </style>
<?= un_htmlchars($cf['headCustom']) ?>