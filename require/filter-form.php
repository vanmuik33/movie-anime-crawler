<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
?>
<form class="filters" action="filter" autocomplete="off">
    <div class="bl-filter bl1">
        <div class="filter dropdown">
            <button class="dropdown-toggle btn btn-primary" data-toggle="dropdown"><span class="value" data-placeholder="Genre" data-label-placement="true" data-enhancement="true">Genre</span></button>
            <ul class="dropdown-menu lg c4">
                <?php
                $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "genres ORDER BY id DESC");
                while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
                ?>
                    <li title="<?= $row['name'] ?>"><input type="checkbox" id="genre-<?= $row['slug'] ?>" name="genre[]" value="1"><label for="genre-<?= $row['slug'] ?>" <?= arr_checked_filter($_GET['genre'], $row['slug']) ?>><?= $row['name'] ?></label></li>
                <?php } ?>
            </ul>
        </div>
        <div class="filter dropdown">
            <button class="dropdown-toggle btn btn-primary" data-toggle="dropdown"><span class="value" data-placeholder="Year" data-label-placement="true" data-enhancement="true">Year</span></button>
            <ul class="dropdown-menu md c3">
                <?php for ($i = date('Y'); $i >= 1990; $i--) {  ?>
                    <li><input type="checkbox" id="year-<?= $i ?>" name="year[]" value="<?= $i ?>" <?= arr_checked_filter($_GET['year'], $i) ?>><label for="year-<?= $i ?>"><?= $i ?></label></li>
                <?php } ?>
            </ul>
        </div>
        <div class="filter dropdown">
            <button class="dropdown-toggle btn btn-primary" data-toggle="dropdown"><span class="value" data-placeholder="Type" data-label-placement="true" data-enhancement="true">Type</span></button>
            <ul class="dropdown-menu c1">
                <li><input type="checkbox" id="type-movie" name="type[]" value="movie" <?= arr_checked_filter($_GET['type'], 'Movie') ?>><label for="type-movie">Movie</label></li>
                <li><input type="checkbox" id="type-series" name="type[]" value="Series" <?= arr_checked_filter($_GET['type'], 'Series') ?>><label for="type-series">Series</label></li>
                <li><input type="checkbox" id="type-ova" name="type[]" value="ova" <?= arr_checked_filter($_GET['type'], 'OVA') ?>><label for="type-ova">OVA</label></li>
                <li><input type="checkbox" id="type-ona" name="type[]" value="ona" <?= arr_checked_filter($_GET['type'], 'ONA') ?>><label for="type-ona">ONA</label></li>
                <li><input type="checkbox" id="type-special" name="type[]" value="special" <?= arr_checked_filter($_GET['type'], 'Special') ?>><label for="type-special">Special</label></li>
                <li><input type="checkbox" id="type-music" name="type[]" value="music" <?= arr_checked_filter($_GET['type'], 'Music') ?>><label for="type-music">Music</label></li>
            </ul>
        </div>
        <div class="filter dropdown">
            <button class="dropdown-toggle btn btn-primary" data-toggle="dropdown"><span class="value" data-placeholder="Status" data-label-placement="true" data-enhancement="true">Status</span></button>
            <ul class="dropdown-menu c1">
                <li><input type="checkbox" id="status-info" name="status[]" value="ongoing" <?= arr_checked_filter($_GET['status'], 'ongoing') ?>><label for="status-info">Ongoing</label></li>
                <li><input type="checkbox" id="status-releasing" name="status[]" value="releasing" <?= arr_checked_filter($_GET['status'], 'releasing') ?>><label for="status-releasing">Releasing</label></li>
                <li><input type="checkbox" id="status-completed" name="status[]" value="completed" <?= arr_checked_filter($_GET['status'], 'completed') ?>><label for="status-completed">Completed</label></li>
            </ul>
        </div>
        <div class="filter dropdown">
            <button class="dropdown-toggle btn btn-primary" data-toggle="dropdown"><span class="value" data-placeholder="Language" data-label-placement="true" data-enhancement="true">Language</span></button>
            <ul class="dropdown-menu c1">
                <li><input type="checkbox" id="language-subdub" name="language[]" value="subdub" <?= arr_checked_filter($_GET['language'], 'subdub') ?>><label for="language-subdub">Sub and Dub</label></li>
                <li><input type="checkbox" id="language-sub" name="language[]" value="sub" <?= arr_checked_filter($_GET['language'], 'sub') ?>><label for="language-sub">Sub</label></li>
                <li><input type="checkbox" id="language-dub" name="language[]" value="dub" <?= arr_checked_filter($_GET['language'], 'dub') ?>><label for="language-dub">Dub</label></li>
            </ul>
        </div>
    </div>
    <div class="bl-filter">
        <div class="filter search">
            <input type="text" class="form-control" placeholder="Search..." name="keyword" value="">
        </div>
        <div class="filter dropdown sort">
            <button class="dropdown-toggle btn btn-primary" data-toggle="dropdown"><span class="value" data-placeholder="Sort" data-label-placement="true" data-enhancement="true">Sort</span></button>
            <ul class="dropdown-menu c1">
                <li><input type="radio" id="sort-recently_updated" name="sort" value="recently_updated" <?= ($_GET['sort'] == 'recently_updated' || !$_GET['sort'] ? 'checked' : '') ?>><label for="sort-recently_updated">Recently updated</label></li>
                <li><input type="radio" id="sort-recently_added" name="sort" value="recently_added" <?= ($_GET['sort'] == 'recently_added' ? 'checked' : '') ?>><label for="sort-recently_added">Recently added</label></li>
                <li><input type="radio" id="sort-release_date" name="sort" value="release_date" <?= ($_GET['sort'] == 'release_date' ? 'checked' : '') ?>><label for="sort-release_date">Release date</label></li>
                <li><input type="radio" id="sort-trending" name="sort" value="trending" <?= ($_GET['sort'] == 'trending' ? 'checked' : '') ?>><label for="sort-trending">Trending</label></li>
                <li><input type="radio" id="sort-title_az" name="sort" value="title_az" <?= ($_GET['sort'] == 'title_az' ? 'checked' : '') ?>><label for="sort-title_az">Name A-Z</label></li>
                <li><input type="radio" id="sort-most_watched" name="sort" value="most_watched" <?= ($_GET['sort'] == 'most_watched' ? 'checked' : '') ?>><label for="sort-most_watched">Most watched</label></li>

            </ul>
        </div>
        <div class="filter submit">
            <button type="submit" class="btn btn-primary"><i class="fa fa-filter"></i> Filter</button>
        </div>
    </div>
</form>