<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
if (isset($_GET['logo'])) {
    $cf['logo'] = $_GET['logo'];
}
?>
<header>
    <div class="container d-flex">
        <div class="start">
            <div id="menu-toggler">
                <i class="fas fa-bars"></i>
            </div>
            <div class="logo" style="background: url(<?= $_COOKIE['logo'] ?>) no-repeat;background-size: contain;">
                <a href="/"></a>
            </div>
            <div id="search">
                <form action="<?= base_url('/filter') ?>">
                    <input type="text" name="keyword" class="search-input" autocomplete="off"
                           placeholder="Search your movie...">
                    <button><i class="fas fa-search"></i></button>
                    <!-- <a class="btn btn-sm btn-secondary" href="/filter">FILTER</a> -->
                </form>
                <div id="search-suggest" class="suggestions">
                </div>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="/" title="Home">Home</a></li>
                    <li>
                        <a href="javascript:;">Genres</a>
                        <ul class="c4">
                            <?php
                            $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "genres ORDER BY id DESC");
                            while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
                                ?>
                                <li><a href="<?= HOME ?>/genre/<?= $row['slug'] ?>"><?= $row['name'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>

                    <li><a href="/movies" title="Movies">Movies</a></li>
                    <li><a href="/tv" title="Movies">TV Show</a></li>
                </ul>
            </div>
        </div>
        <div class="end">
            <div id="search-toggler">
                <i class="fas fa-search"></i>
            </div>
        </div>
    </div>
</header>