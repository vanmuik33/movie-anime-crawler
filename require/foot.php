<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
?>
<footer>
    <div class="bl1">
        <section id="azlist">
            <div class="head">
                <span class="title">A-Z List</span><span class="desc">Searching anime order by alphabet name A to Z.</span>
            </div>
            <div class="body">
                <ul>
                    <li><a href="/az-list/A" rel="nofollow" title="A">A</a></li>
                    <li><a href="/az-list/B" rel="nofollow" title="B">B</a></li>
                    <li><a href="/az-list/C" rel="nofollow" title="C">C</a></li>
                    <li><a href="/az-list/D" rel="nofollow" title="D">D</a></li>
                    <li><a href="/az-list/E" rel="nofollow" title="E">E</a></li>
                    <li><a href="/az-list/F" rel="nofollow" title="F">F</a></li>
                    <li><a href="/az-list/G" rel="nofollow" title="G">G</a></li>
                    <li><a href="/az-list/H" rel="nofollow" title="H">H</a></li>
                    <li><a href="/az-list/I" rel="nofollow" title="I">I</a></li>
                    <li><a href="/az-list/J" rel="nofollow" title="J">J</a></li>
                    <li><a href="/az-list/K" rel="nofollow" title="K">K</a></li>
                    <li><a href="/az-list/L" rel="nofollow" title="L">L</a></li>
                    <li><a href="/az-list/M" rel="nofollow" title="M">M</a></li>
                    <li><a href="/az-list/N" rel="nofollow" title="N">N</a></li>
                    <li><a href="/az-list/O" rel="nofollow" title="O">O</a></li>
                    <li><a href="/az-list/P" rel="nofollow" title="P">P</a></li>
                    <li><a href="/az-list/Q" rel="nofollow" title="Q">Q</a></li>
                    <li><a href="/az-list/R" rel="nofollow" title="R">R</a></li>
                    <li><a href="/az-list/S" rel="nofollow" title="S">S</a></li>
                    <li><a href="/az-list/T" rel="nofollow" title="T">T</a></li>
                    <li><a href="/az-list/U" rel="nofollow" title="U">U</a></li>
                    <li><a href="/az-list/V" rel="nofollow" title="V">V</a></li>
                    <li><a href="/az-list/W" rel="nofollow" title="W">W</a></li>
                    <li><a href="/az-list/X" rel="nofollow" title="X">X</a></li>
                    <li><a href="/az-list/Y" rel="nofollow" title="Y">Y</a></li>
                    <li><a href="/az-list/Z" rel="nofollow" title="Z">Z</a></li>
                </ul>
            </div>
        </section>
        <?php /*<div class="logo" style="background: url(<?= $cf['logo'] ?>) no-repeat;background-size: contain;"></div> */?>
    </div>
    <div class="bl2 text-center">
        <ul class="links">
            <li><a href="/added">New Added</a></li>
            <li><a href="/newest">New Release</a></li>
            <li><a href="/upcoming">Upcoming</a></li>
            <li><a href="/most-watched">Most Watched</a></li>
        </ul>
        <div class="disclaim">
        <?= un_htmlchars($cf['foot_content']) ?>
        </div>
    </div>
</footer>