<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
// $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "vip  ORDER BY name ASC");
// while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
// }
function quangCao($location)
{
    global $mysql;
    $htmlADS = "";
    $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "ads WHERE location = '$location' ORDER BY id DESC");
    while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
        $style = ($row['style'] ? $row['style'] : 'width: 100%;');
        $htmlADS .= '<div class="ngockush-ads">
                        <a style="' . $style . '" href="' . $row['url'] . '" onclick="clickADS(' . $row['id'] . ');" target="_blank">
                            <img src="' . $row['image'] . '">
                        </a>
                    </div>';
    }
    return $htmlADS;
}

function flwItem($row)
{
    $episode_id = get_data_multi('id', "episode", "movieId = {$row['id']}");
    $meta_info = '';
    if ($row['type'] == 'tv') {
        $meta_info = '<div>
                            <span class="season">'. $row['season'] . '</span><span class="dot"></span><span class="ep-status"> EPS '. $row['ep_status'] . '</span>
                        </div>';
    } else {
        $meta_info = '<div>
                            <span class="ep-status">' . $row['year'] . '</span>
                        </div>';
    }
    return '<div class="item">
                <div class="inner ">
                    <div class="type">' . $row['type'] . '</div>
                    <a class="ani poster" href="' . base_url("/film/{$row['slug']}/ep-{$row['id']}") . '" data-tip="">
                        <img src="' . $row['thumb'] . '" alt="' . $row['name'] . '">
                    </a>
                    <div class="info">
                        ' . $meta_info . '
                    </div>
                    <a href="' . base_url("/film/{$row['slug']}") . '" class="name d-title" data-jp="' . $row['name'] . '">' . $row['name'] . '</a>
                </div>
            </div>';
}

function EpisodeHome($row)
{
    $episode_id = get_data_multi('id', "episode", "movieId = {$row['id']}");
    $meta_info = '';
    if ($row['type'] == 'tv') {
        $meta_info = '<div>
                            <span class="season">'. $row['season'] . '</span><span class="dot"></span><span class="ep-status"> EPS '. $row['num'] . '</span>
                        </div>';
    } else {
        $meta_info = '<div>
                            <span class="ep-status">' . $row['year'] . '</span>
                        </div>';
    }
    return '<div class="item">
                <div class="inner ">
                    <div class="type">' . $row['type'] . '</div>
                    <a class="ani poster" href="' . base_url("/film/{$row['slug']}/ep-{$row['epId']}") . '" data-tip="">
                        <img src="' . $row['thumb'] . '" alt="' . $row['name'] . '">
                    </a>
                    <div class="info">
                        ' . $meta_info . '
                    </div>
                    <a href="' . base_url("/film/{$row['slug']}") . '" class="name d-title" data-jp="' . $row['name'] . '">' . $row['name'] . '</a>
                </div>
            </div>';
}

function showMovie($where)
{
    global $mysql;
    $flw = '';
    $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "movie $where");
    while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
        $flw .= flwItem($row);
    }
    return $flw;
}

function showEpisode($where)
{
    global $mysql;
    $flw = '';
    $arr = $mysql->query("SELECT *," . DATABASE_FX . "episode.id as epId FROM " . DATABASE_FX . "episode INNER JOIN " . DATABASE_FX . "movie on " . DATABASE_FX . "episode.movieId = " . DATABASE_FX . "movie.id $where");
    while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
        $flw .= EpisodeHome($row);
    }
    return $flw;
}

function categorySpan($str)
{
    try {
        $arr = json_decode($str, true);
        $name = get_data_multi('name', "genres", "slug = '{$arr[0]}'");
        $text = "<span>$name</span>";
        return $text;
    } catch (\Throwable $th) {
        return '';
    }
}

function filter()
{
    $keyword = sql_escape($_GET['keyword']);
    $sort = sql_escape($_GET['sort']);
    $order = '';
    if ($keyword) {
        $order .= " AND (name LIKE '%$keyword%' OR origin_name LIKE '%$keyword%')";
    }
    if ($_GET['type']) {
        foreach ($_GET['type'] as $type) {
            $type = sql_escape($type);
            $order .= " AND type = '$type'";
        }
    }
    if ($_GET['genre']) {
        foreach ($_GET['genre'] as $genre) {
            $genre = sql_escape($genre);
            $order .= " AND genres LIKE '%\"$genre\"%'";
        }
    }
    if ($_GET['country']) {
        foreach ($_GET['country'] as $country) {
            $country = sql_escape($country);
            $order .= " AND country LIKE '%$country%'";
        }
    }
    if ($_GET['year']) {
        foreach ($_GET['year'] as $year) {
            $year = (int)$year;
            $order .= " AND year LIKE '%$year%'";
        }
    }
    if ($_GET['quality']) {
        foreach ($_GET['quality'] as $quality) {
            $quality = sql_escape($quality);
            $order .= " AND quality = '$quality'";
        }
    }
    if ($sort == 'recently_updated') {
        $order .= " ORDER BY created DESC";
    } elseif ($sort == 'recently_added') {
        $order .= " ORDER BY id DESC";
    } elseif ($sort == 'release_date') {
        $order .= " ORDER BY year DESC";
    } elseif ($sort == 'trending') {
        $order .= " ORDER BY view DESC";
    } elseif ($sort == 'title_az') {
        $order .= " ORDER BY name ASC";
    } elseif ($sort == 'scores') {
        $order .= " ORDER BY votePoint DESC";
    } elseif ($sort == 'imdb') {
        $order .= " ORDER BY imdb DESC";
    } elseif ($sort == 'most_watched') {
        $order .= " ORDER BY view DESC";
    } else $order .= " ORDER BY created DESC";
    return $order;
}

function episode_list($movieId, $start, $limit)
{
    global $mysql;
    $episode_list = '';
    $movie_links = get_data_multi('slug', "movie", "id = $movieId");
    $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "episode WHERE movieId = $movieId ORDER BY num ASC LIMIT $start, $limit");
    while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
        $episode_list .= '<li>
            <a class="ep-item" href="' . base_url("/film/$movie_links/ep-{$row['id']}") . '" data-num="' . $row['num'] . '" data-id="' . $row['id'] . '">' . $row['name'] . '</a>
        </li>';
    }
    return $episode_list;
}
