<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');
$config['db_host']    = 'localhost';
$config['db_name']     = 'moviesjoy';
$config['db_user']    = 'root';
$config['db_pass']    = 'root';
$tb_prefix            = 'table_';
define('SERVER_HOST', $config['db_host']);
define('DATABASE_NAME', $config['db_name']);
define('DATABASE_USER', $config['db_user']);
define('DATABASE_PASS', $config['db_pass']);
define('DATABASE_FX',            'table_');
define('NOW', time());
define('DATE_TO_DAY', date('Y-m-d'));
define('DATE', date('Y-m-d - H:i:s'));
define('HOME', 'https://moviesjoy.local');
define('LEECH_DOMAIN', 'https://moviesjoy.cc');
define('APP_DEBUG', 0);
define('DOCUMENT_ROOT', $_SERVER["DOCUMENT_ROOT"]);
define('UPLOAD_DIR', '/upload');
define('_DIR', DOCUMENT_ROOT);
if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
    $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_CF_CONNECTING_IP'];
}
define('IP', $_SERVER['REMOTE_ADDR']);
define('USER_AGENT', $_SERVER['HTTP_USER_AGENT']);
define('URL_LOAD', HOME . $_SERVER["REQUEST_URI"]);
include('init.php');