<?php
function jsonDecode($str)
{
    if (!$str) return [];
    $str = str_replace("\n", '', $str);
    $str = str_replace("\r", '', $str);
    $str = str_replace("\n\r", '', $str);
    $str = str_replace("\r\n", '', $str);
    return json_decode($str, true);
}
function jsonEncode($data)
{
    $data = json_encode($data, JSON_UNESCAPED_UNICODE);
    $data = str_replace("'", "\'", $data);
    return $data;
}
function movie_last($movieId)
{
    $data = jsonDecode(get_data_multi('data', 'episode', "movieId = $movieId"));

    if (count($data) <= 0) return ["season_name" => '', "episode_name" => ''];

    $season = array_key_last($data);
    $episode = array_key_last($data[$season]['season_episode']);
    return ["season_name" => $data[$season]['season_name'], "episode_name" => $data[$season]['season_episode'][$episode]['episode_name']];
}
function checked($a, $b)
{
    return ($a == $b ? 'checked' : '');
}
function arr_checked_filter($arr, $b)
{
    foreach ($arr as $a) {
        if ($a == $b) {
            return 'checked';
            break;
        }
    }
}
function historyWatching($movieId)
{
    global $users, $mysql;
    if (get_total('movie', "WHERE id = $movieId") <= 0) return;
    if (!$_SESSION['auth']) return;
    $his = jsonDecode($users['historyWatching']);
    if (!in_array($movieId, $his)) {
        $his[] = (int)$movieId;
        $his = jsonEncode($his);
        $mysql->update('users', "historyWatching = '$his'", "id = {$users['id']}");
    }
}
function filter_pages()
{
    $params = $_GET;
    $queryString = '';
    $n = 0;
    foreach ($params as $key => $value) {
        if ($key != 'page') {
            if (is_array($value)) {
                foreach ($value as $item) {
                    $queryString .= ($n == 0 ? '?' : '&') . $key . '[]=' . urlencode($item);
                }
            } else {
                $queryString .= ($n == 0 ? '?' : '&') . $key . '=' . urlencode($value);
            }
        }
        $n++;
    }
    return $queryString . '&page=';
}

function matchDomain($url) {
    // Check if the URL starts with "http://" or "https://"
    if (preg_match('/^https?:\/\//i', $url) === 1) {
        // If it does, return the URL
        return $url;
    } else {
        return LEECH_DOMAIN . '/' . $url;
    }
}