<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
function web_name()
{
	return ucfirst(explode('://', HOME)[1]);
}
function base_url($url = "")
{
	return HOME . $url;
}
function selected($a, $b)
{
	return ($a == $b ? 'selected' : '');
}
function filter_genres($genres)
{
	$genre = explode('_', $_GET['genre']);
	foreach ($genre as $k => $v) {
		if ($v == $genres) {
			return 'checked';
			break;
		}
	}
}
function filter_country($country)
{
	$a = explode('_', $_GET['country']);
	foreach ($a as $k => $v) {
		if ($v == $country) {
			return 'checked';
			break;
		}
	}
}
function cut_html($begin, $end, $str)
{
	$result = explode($end, explode($begin, $str)[1])[0];
	return $result;
}
function comicRate($rate, $count)
{
	if ($count <= 0) return 0;
	if ($rate <= 0) return 0;
	if ($count >= 1 && $rate >= 1) {
		$total = (($rate / $count) / 10) * 100;
		$totla_users_score = round($rate / $count, 2);
	} else {
		$totla_users_score = $total = $count = 0;
	}
	return $totla_users_score;
}
function siteBBcode($type, $arr = [])
{
	global $cf;
	$str = get_data_multi($type, 'advanced', "pageType = '{$arr['pageType']}'");
	$str = str_replace('{web_title}', $cf['title'], $str);
	$str = str_replace('{web_desc}', $cf['description'], $str);
	$str = str_replace('{web_keywords}', $cf['keywords'], $str);
	$str = str_replace('{web_year}', date('Y'), $str);
	$str = str_replace('{web_name}', web_name(), $str);
	if ($arr['pageType'] == 'Diễn viên') {
		$str = str_replace('{name_cast}', $arr['name_cast'], $str);
	} else if ($arr['pageType'] == 'Quốc gia') {
		$str = str_replace('{name_country}', $arr['name_country'], $str);
	} else if ($arr['pageType'] == 'Thể loại') {
		$str = str_replace('{name_genres}', $arr['name_genres'], $str);
	} else if ($arr['pageType'] == 'Đạo diễn') {
		$str = str_replace('{name_director}', $arr['name_director'], $str);
	} else if ($arr['pageType'] == 'Tìm kiếm') {
		$str = str_replace('{keywords}', $arr['keywords'], $str);
	} else if ($arr['pageType'] == 'Thông tin phim' || $arr['pageType'] == 'Xem phim') {
		$str = str_replace('{movie_name}', $arr['movie_name'], $str);
		$str = str_replace('{movie_year}', $arr['movie_year'], $str);
		$str = str_replace('{movie_country}', $arr['movie_country'], $str);
		$str = str_replace('{movie_quanlity}', $arr['movie_quanlity'], $str);
		$str = str_replace('{movie_orther_name}', $arr['movie_orther_name'], $str);
	}
	return $str;
}
function BBcode($arr = [])
{
	return [
		"title" => siteBBcode('title', $arr),
		"description" => siteBBcode('description', $arr),
		"keywords" => siteBBcode('keyword', $arr)
	];
}
function rightSide($arr)
{

	$html = "";
	foreach ($arr as $k => $v) {
		$html .= '<div class="rightBox full">
					<div class="barTitle">' . $v['name'] . '</div>
					<div class="barContent">
						<div class="arrow-general"></div>
						<div>
						' . $v['data'] . '
						</div>
					</div>
				</div>';
	}
	return '<div id="rightside">' . $html . '</div>';
}
function value_detect($k, $v, $table)
{
	switch ($k) {
		case 'slug':
			return chuyenslug($v) . ($table == 'movie' ? "-" . (int)get_data_multi('id', 'movie', "id >= 1 ORDER BY id DESC") + 1 : '');
		case 'content':
			return text_insert(htmlchars($v));
		case 'name':
			return text_insert(htmlchars($v));
		case 'origin_name':
			return text_insert(htmlchars($v));
		case 'html':
			return text_insert(htmlchars($v));
		case 'homeContent':
			return text_insert(htmlchars($v));
		case 'privacyPolicy':
			return text_insert(htmlchars($v));
		case 'genres':
			return json_encode($v, JSON_UNESCAPED_UNICODE);
		case 'Casts':
			return json_convert($v);
		case 'Production':
			return json_convert($v);
		default:
			return ($v != '' ? $v : NULL);
	}
}
function episodeCheck($arr, $ep_slug)
{
	foreach ($arr as $k => $v) {
		if ($v['ep_slug'] == $ep_slug) return ["data" => $v, "ep_num" => $k];
	}
	return [];
}
function arr_selected($json, $id)
{
	foreach (json_decode($json, true) as $slug) {
		if ($id == $slug) {
			return 'selected';
			break;
		}
	}
}
function arr_include($arr, $id)
{
	foreach ($arr as $slug) {
		if ($id == $slug) {
			return 'include';
			break;
		}
	}
}
function json_convert($json)
{
	if ($json == '') return '[]';
	try {
		$arr = array();
		foreach (json_decode($json, true) as $v) {
			$arr[] = $v['value'];
		}
		return json_encode($arr, JSON_UNESCAPED_UNICODE);
	} catch (\Throwable $th) {
		return '[]';
	}
}

function base_admin($url = "")
{
	return HOME . "/dashboard$url";
}
function importFile($file, $st = NULL)
{
	if (!file_exists(DOCUMENT_ROOT . $file . ".php")) die(header('HTTP/1.0 404 Not Found', true, 404));
	global $mysql, $cf, $mysqldb, $users, $phpfastcache;
	require_once(DOCUMENT_ROOT . $file . ".php");
}

function icon($key, $w, $h)
{
	return '<img src="' . base_url("/upload/icon/$key") . '" style="width:' . $w . 'px;height:' . $h . 'px;"/>';
}
function MessageJson($status, $message)
{
	return json_encode(array(
		'status' => $status,
		'message' => $message
	));
}
function SaveTxt($filename, $content)
{
	$myfile = fopen($filename, "w") or die("Unable to open file!");
	fwrite($myfile, mb_convert_encoding($content, 'UTF-8'));
	fclose($myfile);
}

function format_numb($amount)
{
	return number_format($amount);
}
function episodeJson($txt)
{
	if (!$txt) return [];
	$txt = str_replace("\n", "", $txt);
	$txt = str_replace("\n\r", "", $txt);
	$txt = str_replace("\r\n", "", $txt);
	$txt = str_replace("\r", "", $txt);
	return json_decode($txt, true);
}
function text_remove_space($txt)
{
	$txt = str_replace("\n", "", $txt);
	$txt = str_replace("\n\r", "", $txt);
	$txt = str_replace("\r\n", "", $txt);
	$txt = str_replace("\r", "", $txt);
	return $txt;
}
function movieGenres($txt)
{
	if (!$txt) return 'N/A';;
	$arr = json_decode($txt, true);
	$countCate = count($arr);
	if ($countCate <= 0) return 'N/A';;
	$a = '';
	foreach ($arr as $k => $v) {
		$name = get_data_multi('name', 'genres', "slug = '$v'");
		$a .= '<a class="dotUnder" title="' . $name . '" href="' . base_url("/Genre/$v") . '">' . $name . '</a>' . (($countCate - 1) > $k ? ', ' : '');
	}
	return $a;
}
function movieGenres_sideRight($txt, $movie_slug)
{
	global $mysql;
	if (!$txt) return '';
	$arr = json_decode($txt, true);
	$countCate = count($arr);
	if ($countCate <= 0) return '';
	$feed = '';
	foreach ($arr as $k => $v) {
		$feed .= " OR genres LIKE '%\"$v\"%'";
	}
	$result = '<div class="series_links full related_list">';
	$arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "movie WHERE public >= 1 AND slug != '$movie_slug' $feed ORDER BY time DESC LIMIT 16");
	while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
		$result = '<h4 class="tit">
						<a title="' . $row['name'] . '" href="' . base_url("/Cartoon/{$row['slug']}") . '">Watch ' . $row['name'] . ' online free</a>
					</h4>';
	}
	$result .= '</div>';
	return $result;
}
function json_to_href($str, $url, $table = false)
{
	try {
		$arr = jsonDecode($str);
		$count = count($arr);
		$a = '';
		foreach ($arr as $k => $v) {
			$name = ($table ? get_data_multi('name', $table, "slug = '$v'") : $v);
			if ($table == false) {
				$v = str_replace(' ', '-', $v);
			}
			$a .= '<a href="' . base_url($url . $v) . '">' . $name . '</a>' . (($count - 1) > $k ? ', ' : '');
		}
		return $a ? $a : 'N/A';
	} catch (\Throwable $th) {
		return 'N/A';
	}
}
function json_to_text($str)
{
	try {
		$arr = jsonDecode($str);
		$count = count($arr);
		$text = '';
		foreach ($arr as $k => $v) {
			$text .= $v . (($count - 1) > $k ? ',' : '');
		}
		return $text;
	} catch (\Throwable $th) {
		return 'N/A';
	}
}
function CheckCapcha($captcha, $secret_key)
{
	$response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secret_key) .  '&response=' . urlencode($captcha));
	$responseKeys = json_decode($response, true);
	return ($responseKeys["success"] ? true : false);
}
function checkRecaptchaV3($responseToken, $secretKey)
{
	// Gửi yêu cầu POST đến API của Google reCAPTCHA
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array(
		'secret' => $secretKey,
		'response' => $responseToken
	);
	$options = array(
		'http' => array(
			'method' => 'POST',
			'header' => 'Content-Type: application/x-www-form-urlencoded\r\n',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);

	// Phân tích kết quả trả về từ API
	$response = json_decode($result);
	if ($response == null || !$response->success) {
		// Không hợp lệ
		return false;
	}
	return true;
}

function resize_crop_image($source_file, $dst_dir, $quality = 80)
{
	$imgsize = getimagesize($source_file);
	$mime = $imgsize['mime'];

	switch ($mime) {
		case 'image/gif':
			$image_create = "imagecreatefromgif";
			$image = "imagegif";
			break;
		case 'image/png':
			$image_create = "imagecreatefrompng";
			$image = "imagepng";
			$quality = 7;
			break;
		case 'image/jpeg':
			$image_create = "imagecreatefromjpeg";
			$image = "imagejpeg";
			$quality = 80;
			break;
		default:
			return false;
			break;
	}
	$src_img = $image_create($source_file);
	$image($src_img, $dst_dir, $quality);
	if ($src_img) {
		imagedestroy($src_img);
		return true;
	}
}

if (!function_exists("array_key_last")) {
	function array_key_last($array)
	{
		// For PHP >= 7.3
		// if ($key === array_key_first($array))
		//     echo 'FIRST ELEMENT!';

		// if ($key === array_key_last($array))
		//     echo 'LAST ELEMENT!';

		//For PHP <= 7.3
		if (!is_array($array) || empty($array)) {
			return NULL;
		}

		return array_keys($array)[count($array)];
	}
}
function view_pages($ttrow, $limit, $page, $link)
{

	$total = ceil($ttrow / $limit);

	if ($total <= 1) {

		return;
	}

	$main .= " <nav aria-label=\"...\">
    <ul class=\"pagination pagination-primary\">";

	if ($page <> 1) {

		// $main .= "<li class='page-item'><a class=\"page-link\" href=\"$url".$link."1\" tabindex=\"1\">Đầu</a></li>";

		$main .= "<li class=\"page-item\"><a class=\"page-link\" href=\"$link" . ($page - 1) . "\" tabindex=\"-1\" data-original-title=\"\" title=\"\">Trước</a></li>";
	}

	for ($num = 1; $num <= $total; $num++) {

		if ($num < $page - 1 || $num > $page + 4) {

			continue;
		}

		if ($num == $page) {

			$main .= "<li class=\"page-item active\"><a class=\"page-link\" href=\"#$num\" data-original-title=\"\" title=\"\">$num <span class=\"sr-only\">(current)</span></a></li>";
		} else {

			$main .= "<li class=\"page-item\"><a class=\"page-link\" href=\"$link$num\" data-original-title=\"\" title=\"\">$num</a></li>";
		}
	}

	$main .= "<li class=\"page-item disabled\">
  
    <a class=\"page-link\" href=\"#\" tabindex=\"-1\">&hellip;</a>
  
    </li>";

	if ($page <> $total) {

		$main .= "<li class=\"page-item\"><a class=\"page-link\" href=\"$link" . $total . "\" data-original-title=\"\" title=\"\">Cuối</a></li>";
	}

	$main .= '  </ul>
    </nav>';

	return $main;
}

function movie_navition($ttrow, $limit, $page, $link)
{

	$total = ceil($ttrow / $limit);
	if ($total <= 1) {
		return;
	}
	$main = '';
	$main .= '<div class="d-flex justify-content-center">
	<nav>
		<ul class="pagination">';

	if ($page <> 1) {

		$main .= '<li class="page-item"><a title="First" href="' . $link . '1" class="page-link"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>';

		$main .= '<li class="page-item"><a title="Previous" href="' . $link . ($page - 1) . '" class="page-link"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>';
	}

	for ($num = 1; $num <= $total; $num++) {

		if ($num < $page - 1 || $num > $page + 4) {

			continue;
		}

		if ($num == $page) {

			$main .= '<li class="page-item active"><a class="page-link">' . number_format($num) . '</a></li>';
		} else {
			$main .= ' <li class="page-item"><a title="Page ' . $num . '" href="' . $link . $num . '" class="page-link">' . number_format($num) . '</a></li>';
		}
	}
	//$main .= '<li class="page-item disabled"><a title="Next" href="#" class="page-link">&hellip;</a></li>';
	if ($page <> $total) {
		$main .= '<li class="page-item"><a title="Next" href="' . $link . ($page + 1) . '" class="page-link"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>';
		$main .= '<li class="page-item"><a title="Last" href="' . $link . $total . '" class="page-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>';
	}
	$main .= '	</ul>
	</nav>
</div>';
	return $main;
}
function page_checker($table, $f2, $limit, $page)
{
	$total_records = get_total($table, $f2);
	$current_page = $page ? $page : 1;
	$total_page = ceil($total_records / $limit);
	if ($current_page > $total_page) {
		$current_page = $total_page;
	} else if ($current_page < 1) {
		$current_page = 1;
	}
	$start = ($current_page - 1) * $limit;
	return array(
		'start' => $start,
		'total' => $total_records,
		'total_page' => $total_page
	);
}
function chuyenslug($str)
{
	$str = trim(mb_strtolower($str));
	$str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
	$str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
	$str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
	$str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
	$str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
	$str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
	$str = preg_replace('/(đ)/', 'd', $str);
	$str = preg_replace('/[^a-z0-9-\s]/', '', $str);
	$str = preg_replace('/([\s]+)/', '-', $str);
	return $str;
}
function XoaDauPhay($str)
{
	$str = preg_replace('/,/', '', $str);
	return $str;
}
function get_ascii($str)
{

	$chars = array(

		'a'	=>	array('ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'á', 'à', 'ả', 'ã', 'ạ', 'â', 'ă', 'Á', 'À', 'Ả', 'Ã', 'Ạ', 'Â', 'Ă'),

		'e' 	=>	array('ế', 'ề', 'ể', 'ễ', 'ệ', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê'),

		'i'	=>	array('í', 'ì', 'ỉ', 'ĩ', 'ị', 'Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị'),

		'o'	=>	array('ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'Ố', 'Ồ', 'Ổ', 'Ô', 'Ộ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ơ', 'Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ơ'),

		'u'	=>	array('ứ', 'ừ', 'ử', 'ữ', 'ự', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự', 'ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư', 'Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ', 'Ư'),

		'y'	=>	array('ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ', 'Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ'),

		'd'	=>	array('đ', 'Đ'),

	);

	foreach ($chars as $key => $arr)

		foreach ($arr as $val)

			$str = str_replace($val, $key, $str);

	return $str;
}


function Khoangtrang($string)
{
	$name = preg_replace('/\s+/', '', $string);
	return $name;
}
function getCurURL()
{
	if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
		$pageURL = "https://";
	} else {
		$pageURL = 'http://';
	}
	if (isset($_SERVER["SERVER_PORT"]) && $_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

function htmltxt($document)
{
	$search = array(
		'@<script???[^>]*?>.*?</script???>@si',  // Strip out javascript
		'@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
		'@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
		'@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
	);
	$text = preg_replace($search, '', $document);
	return $text;
}
function text_insert($str)
{
	$str = str_replace("'", "\'", $str);
	return $str;
}
function un_htmlchars($str)
{

	return str_replace(array('&lt;', '&gt;', '&quot;', '&amp;', '&#92;', '&#39'), array('<', '>', '"', '&', chr(92), chr(39)), $str);
}

function htmlchars($str)
{

	return str_replace(

		array('&', '<', '>', '"', chr(92), chr(39)),

		array('&amp;', '&lt;', '&gt;', '&quot;', '&#92;', '&#39'),

		$str

	);
}

function injection($str)
{

	$chars = array('chr(', 'chr=', 'chr%20', '%20chr', 'wget%20', '%20wget', 'wget(', 'cmd=', '%20cmd', 'cmd%20', 'rush=', '%20rush', 'rush%20', 'union%20', '%20union', 'union(', 'union=', 'echr(', '%20echr', 'echr%20', 'echr=', 'esystem(', 'esystem%20', 'cp%20', '%20cp', 'cp(', 'mdir%20', '%20mdir', 'mdir(', 'mcd%20', 'mrd%20', 'rm%20', '%20mcd', '%20mrd', '%20rm', 'mcd(', 'mrd(', 'rm(', 'mcd=', 'mrd=', 'mv%20', 'rmdir%20', 'mv(', 'rmdir(', 'chmod(', 'chmod%20', '%20chmod', 'chmod(', 'chmod=', 'chown%20', 'chgrp%20', 'chown(', 'chgrp(', 'locate%20', 'grep%20', 'locate(', 'grep(', 'diff%20', 'kill%20', 'kill(', 'killall', 'passwd%20', '%20passwd', 'passwd(', 'telnet%20', 'vi(', 'vi%20', 'insert%20into', 'select%20', 'nigga(', '%20nigga', 'nigga%20', 'fopen', 'fwrite', '%20like', 'like%20', '$_request', '$_get', '$request', '$get', '.system', 'HTTP_PHP', '&aim', '%20getenv', 'getenv%20', 'new_password', '&icq', '/etc/password', '/etc/shadow', '/etc/groups', '/etc/gshadow', 'HTTP_USER_AGENT', 'HTTP_HOST', '/bin/ps', 'wget%20', 'uname\x20-a', '/usr/bin/id', '/bin/echo', '/bin/kill', '/bin/', '/chgrp', '/chown', '/usr/bin', 'g\+\+', 'bin/python', 'bin/tclsh', 'bin/nasm', 'perl%20', 'traceroute%20', 'ping%20', '.pl', '/usr/X11R6/bin/xterm', 'lsof%20', '/bin/mail', '.conf', 'motd%20', 'HTTP/1.', '.inc.php', 'config.php', 'cgi-', '.eml', 'file\://', 'window.open', '<SCRIPT>', 'javascript\://', 'img src', 'img%20src', '.jsp', 'ftp.exe', 'xp_enumdsn', 'xp_availablemedia', 'xp_filelist', 'xp_cmdshell', 'nc.exe', '.htpasswd', 'servlet', '/etc/passwd', 'wwwacl', '~root', '~ftp', '.js', '.jsp', 'admin_', '.history', 'bash_history', '.bash_history', '~nobody', 'server-info', 'server-status', 'reboot%20', 'halt%20', 'powerdown%20', '/home/ftp', '/home/www', 'secure_site, ok', 'chunked', 'org.apache', '/servlet/con', '<script', '/robot.txt', '/perl', 'mod_gzip_status', 'db_mysql.inc', '.inc', 'select%20from', 'select from', 'drop%20', '.system', 'getenv', 'http_', '_php', 'php_', 'phpinfo()', '<?php', '?>', 'sql=', '\'');

	foreach ($chars as $key => $arr)

		$str = str_replace($arr, '*', $str);

	return $str;
}

function sql_escape($value)
{
	$value = trim(htmlchars(stripslashes(urldecode(injection($value)))));
	return $value;
}
function RemainTime($timestamp, $detailLevel = 1)
{

	$periods = array("giây", "phút", "giờ", "ngày", "tuần", "tháng", "năm", "thập kỷ");
	$lengths = array("60", "60", "24", "7", "4.35", "12", "10");

	$now = time();

	// check validity of date
	if (empty($timestamp)) {
		return "Unknown time";
	}

	// is it future date or past date
	if ($now > $timestamp) {
		$difference = $now - $timestamp;
		$tense = "trước";
	} else {
		$difference = $timestamp - $now;
		$tense = "from now";
	}

	if ($difference == 0) {
		return "vài giây trước";
	}

	$remainders = array();

	for ($j = 0; $j < count($lengths); $j++) {
		$remainders[$j] = floor(fmod($difference, $lengths[$j]));
		$difference = floor($difference / $lengths[$j]);
	}

	$difference = round($difference);

	$remainders[] = $difference;

	$string = "";

	for ($i = count($remainders) - 1; $i >= 0; $i--) {
		if ($remainders[$i]) {
			$string .= $remainders[$i] . " " . $periods[$i];

			if ($remainders[$i] != 1) {
				$string .= "";
			}

			$string .= " ";

			$detailLevel--;

			if ($detailLevel <= 0) {
				break;
			}
		}
	}

	return $string . $tense;
}

function IpAdress()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip_address = $_SERVER['HTTP_CLIENT_IP'];
	}
	//Kiểm tra xem IP có phải là từ Proxy  
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	//Kiểm tra xem IP có phải là từ Remote Address  
	else {
		$ip_address = $_SERVER['REMOTE_ADDR'];
	}
	return $ip_address;
}
function validateUsername($username)
{
	$pattern = '/^[a-zA-Z0-9]{3,20}$/';
	if (preg_match($pattern, $username)) {
		return true;
	}
	return false;
}
function tokenString($length)
{
	$str = "";
	$characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;
}

function encrypt_decrypt($action, $string)
{
	$output         = false;
	$encrypt_method = "AES-256-CBC";
	$secret_key     = 'hackcaigi';
	$secret_iv      = 'hacklamcho';
	$key            = hash('sha256', $secret_key);
	$iv             = substr(hash('sha256', $secret_iv), 0, 16);
	if ($action == 'encrypt') {
		$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
		$output = base64_encode($output);
	} else if ($action == 'decrypt') {
		$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	}
	return $output;
}
function unescapeUTF8EscapeSeq($str)
{
	return preg_replace_callback("/\\\u([0-9a-f]{4})/i", create_function('$matches', 'return html_entity_decode(\'&#x\'.$matches[1].\';\', ENT_QUOTES, \'UTF-8\');'), $str);
}

function RemoveHtml($document)
{
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Chứa javascript
		'@<[\/\!]*?[^<>]*?>@si', // Chứa các thẻ HTML
		'@<style[^>]*?>.*?</style>@siU', // Chứa các thẻ style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Xóa toàn bộ dữ liệu bên trong các dấu ngoặc "<" và ">"
	);
	$text   = preg_replace($search, '', $document);
	$text   = strip_tags($text);
	$text   = trim($text);
	return $text;
}
function is_mobile()
{
	if (isset($_SERVER['HTTP_X_WAP_PROFILE']))
		return true;
	if (preg_match('/wap.|.wap/i', $_SERVER['HTTP_ACCEPT']))
		return true;

	if (isset($_SERVER['HTTP_USER_AGENT'])) {
		$user_agents = array(
			'midp', 'j2me', 'avantg', 'docomo', 'novarra', 'palmos',
			'palmsource', '240x320', 'opwv', 'chtml', 'pda',
			'mmp\/', 'blackberry', 'mib\/', 'symbian', 'wireless', 'nokia',
			'cdm', 'up.b', 'audio', 'SIE-', 'SEC-',
			'samsung', 'mot-', 'mitsu', 'sagem', 'sony', 'alcatel',
			'lg', 'erics', 'vx', 'NEC', 'philips', 'mmm', 'xx', 'panasonic',
			'sharp', 'wap', 'sch', 'rover', 'pocket', 'benq', 'java', 'pt',
			'pg', 'vox', 'amoi', 'bird', 'compal', 'kg', 'voda', 'sany',
			'kdd', 'dbt', 'sendo', 'sgh', 'gradi', 'jb', 'dddi', 'moto', 'ipad', 'iphone', 'Opera Mobi', 'android'
		);
		$user_agents = implode('|', $user_agents);
		if (preg_match("/$user_agents/i", $_SERVER['HTTP_USER_AGENT']))
			return true;
	}

	return false;
}
function vn_to_str($str)
{

	$unicode = array(

		'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

		'd' => 'đ',

		'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

		'i' => 'í|ì|ỉ|ĩ|ị',

		'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

		'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

		'y' => 'ý|ỳ|ỷ|ỹ|ỵ',

		'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

		'D' => 'Đ',

		'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

		'I' => 'Í|Ì|Ỉ|Ĩ|Ị',

		'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

		'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

		'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

	);

	foreach ($unicode as $nonUnicode => $uni) {

		$str = preg_replace("/($uni)/i", $nonUnicode, $str);
	}
	$str = str_replace(' ', ' ', $str);

	return $str;
}
function ngockush_build_iframe($src)
{
	return '<iframe src="' . $src . '" width="100%" height="100%" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>';
}
