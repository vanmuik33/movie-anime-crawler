var tmploading = '<div class="loading-relative"><div class="loading"><div class="span1"></div><div class="span2"></div><div class="span3"></div></div><br />';
var movieId = $('#watch-page').data('id');
var playobj;

function loadEpisodes() {
    var eobj = $('#episodes');
    eobj.html(tmploading);
    if (eobj.length) {
        $.get('/ajax/episode/list-episode?movieId=' + movieId, function(res) {

            eobj.html(res.html);

            loadPageEpisode();

            $('.ep-range').hide();
            $('.ep-item').removeClass('active');
            $('.ep-item[data-id=' + episode_play.id + ']').addClass('active');




            $('.play').on('click', function() {
                const current_url = $('.ep-item[data-id=' + episode_play.id + ']').attr('href');
                PlayEpisode(episode_play.id, current_url);
            });

            var epcrli = $('.ep-item.active').parent();
            var epcrul = epcrli.parent();
            $('.dropdown-item[data-value=' + epcrul.data('range') + ']').click();

            $('.ep-item').on('click', function() {
                const ep_item = $(this);
                const episode_id = ep_item.attr('data-id');
                const current_url = ep_item.attr('href');
                PlayEpisode(episode_id, current_url);
                $('.ep-item').removeClass('active');
                ep_item.addClass('active');

                return false;
            });
        }, 'json');
    }
}


function loadDisqus() {
    var comments = $('#comments');

    if (comments.length) {

        console.log("DISQUS init");
        var disqus_config = function() {
            this.page.url = comments.data('link');
        };

        (function() {

            var d = document,
                s = d.createElement('script');

            s.src = '//' + comments.data('src') + '/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();

    }
}




var PlayEpisode = async function(episode_id, current_url) {
    $('#player').html(tmploading);
    const {
        status,
        message
    } = await $.post('/ajax/episode/player?episode_id=' + episode_id);
    if (status == false) {
        $('.alert-player').show().html(message);
        return;
    }
    $('#player').html('<iframe src="' + message[0].server_link + '" allow="autoplay; fullscreen" allowfullscreen="yes" scrolling="no" style="width: 100%; height: 100%; overflow: hidden;" frameborder="no"></iframe>');
    window.history.pushState('', '', current_url);
    $('html, body').animate({
        scrollTop: $('#player').offset().top
    }, 900);
    $.post('/ajax/movie/view?movieId=' + movieId, function(res) {});
}

function loadPageEpisode() {
    $(document).on("click", ".dropdown-item", function() {
        $('.dropdown-item').removeClass('active');
        $(this).addClass('active');
        var page = $(this).text().trim();
        $('#current-page').text(page);
        $('.ep-range').hide();
        $('.ep-range[data-range=' + page + ']').show();
    });
}

$(document).ready(function() {
    $('#top-anime .tabs .tab').on('click', function() {
        $('#top-anime .tabs .tab').removeClass('active');
        $('#top-anime .body').html(tmploading);
        var type_tab = $(this).attr('data-name');

        $.get('/ajax/movie/top?type=' + type_tab, function(res) {
            $('#top-anime .body').html(res);
        });

        $(this).addClass('active');
        return false;
    });

    $('#top-anime .tabs').find('span[data-name=day]').click();
    try {
        var amungId = $('html').data('aid');
        var url = encodeURIComponent(window.location.href);
        $.get("https://whos.amung.us/pingjs/?k=".concat(amungId, "&c=s&x=").concat(url, "&v=29&r=").concat(Math.ceil(Math.random() * 9999)));
    } catch (err) {}

    $(".light").click(function(e) {
        $("#mask-overlay").toggleClass("active");
        $('#player-wrapper').css('z-index', '103');
    });
    $("#mask-overlay").click(function(e) {
        $("#mask-overlay").removeClass("active");
    });

    $('#menu-toggler').click(function() {
        $('#menu').toggle();
    });

    $('#search-toggler').click(function() {
        $('#search').toggle();
    });


    var hidden_results = true;
    $('.suggestions').mouseover(function() {
        hidden_results = false;
    });
    $('.suggestions').mouseout(function() {
        hidden_results = true;
    });
    var timeout = null;

    $('.search-input').keyup(function() {
        if (timeout != null) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(function() {
            timeout = null;
            var keyword = $('.search-input').val().trim();
            if (keyword.length > 1) {
                $('.suggestions').show();

                $.get("/ajax/search/suggest?keyword=" + keyword, function(res) {
                    $('.suggestions').html(res.html);
                    $('.suggestions').slideUp('fast');
                    $('.suggestions').slideDown('fast');
                    $('#search-loading').hide();
                });
            } else {
                $('.suggestions').hide();
            }
        }, 500);
    });
    $('.search-input').blur(function() {
        if (hidden_results) {
            $('.suggestions').slideUp('fast');
        }
    });


    var swiper = new Swiper("#hotest", {
        slidesPerView: 3,
        spaceBetween: 0,
        autoplay: 2000,
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            900: {
                slidesPerView: 2,
                spaceBetween: 0,
            },
            1320: {
                slidesPerView: 3,
                spaceBetween: 0,
            },
            1880: {
                slidesPerView: 3,
                spaceBetween: 0,
            }
        }

    });

    loadEpisodes();
    loadDisqus();

    $(document).on('keyup', '#search-ep', function(e) {

        e.preventDefault();
        var value = e.target.value;
        //alert(value);
        $('.ep-range li a').removeClass('highlight');
        if (value) {
            var epEl = $('.ep-range li a[data-num=' + value + ']');
            if (epEl.length > 0) {
                var parent = epEl.parent();
                parent = parent.parent();
                console.log(parent);
                //console.log('.dropdown-item[data-value=' + parent.data('range') + ']');

                $('.dropdown-item[data-value=' + parent.data('range') + ']').click();
                if (e.keyCode === 13) {
                    $(e.target).val("");
                    epEl.click();
                } else {
                    epEl.addClass('highlight');
                }
            }
        } else {
            //var currPage = $('.ep-item.active').parent().data('page');
            // $('.ep-page-item[data-page=' + currPage + ']').click();
        }
    });


});