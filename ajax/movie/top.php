<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
$type = $_GET['type'] ? sql_escape($_GET['type']) : die();
if ($type == 'day') {
    $filter = 'view_day';
} else if ($type == 'day') {
    $filter = 'view_week';
} else if ($type == 'day') {
    $filter = 'view_month';
} else {
    $filter = 'view';
}
?>
<div class="tab-content topv items" style="">
    <?php
    $arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "movie ORDER BY $filter DESC LIMIT 10");
    while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
        $episode_id = get_data_multi('id', "episode", "movieId = {$row['id']}");
    ?>
        <a class="item" href="<?= base_url("/film/{$row['slug']}/ep-$episode_id") ?>">
            <div class="poster">
                <span style="background-image:url('<?= $row['thumb'] ?>')"></span>
            </div>
            <div class="info">
                <div class="name d-title" data-jp="<?= $row['name'] ?>"><?= $row['name'] ?></div>
                <div class="meta">
                    <span class="dot">
                        <i class="fas fa-eye"></i> <?= number_format($row[$filter]) ?> </span>
                </div>
            </div>
        </a>
    <?php } ?>
</div>