<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
header('Content-Type: application/json');
$keyword = sql_escape($_GET['keyword']);
$kw_slug = chuyenslug($_GET['keyword']);
$search_result = '';
$arr = $mysql->query("SELECT * FROM " . DATABASE_FX . "movie WHERE name LIKE '%$keyword%' OR origin_name LIKE '%$keyword%' OR slug LIKE '%$kw_slug%' ORDER BY id DESC LIMIT 10");
while ($row = $arr->fetch(PDO::FETCH_ASSOC)) {
    $episode_id = get_data_multi('id', "episode", "movieId = {$row['id']}");
    $search_result .= '<a class="item" href="' . base_url("/anime/{$row['slug']}/ep-$episode_id") . '">
    <div class="poster">
        <span><img src="' . $row['thumb'] . '"></span>
    </div>
    <div class="info">
        <div class="name" data-jp="' . $row['name'] . '">' . $row['name'] . '</div>
        <div class="meta">
            <span class="dot">' . $row['type'] . '</span>
            <span class="dot">' . $row['year'] . '</span>
            <span class="dot">' . $row['status'] . '</span>
        </div>
    </div>
    </a>';
}
$search_result .= '<a href="/filter?keyword=' . $keyword . '" class="more">View all</a>';
die(json_encode([
    "html" => $search_result,
    "status" => true
]));
