<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
header('Content-Type: application/json');
$episode_id = ($_GET['episode_id'] ? (int)$_GET['episode_id'] : die(MessageJson(false, 'Episode ID not found')));
if (get_total('episode', "WHERE id = $episode_id") <= 0) die(MessageJson(false, 'Episode not found'));
$episode_data = get_data_multi('data', 'episode', "id = $episode_id");
$data_decode = json_decode($episode_data);
$data_decode[0]->server_link = matchDomain($data_decode[0]->server_link);
$episode_data = json_encode($data_decode);
die(MessageJson(true, json_decode($episode_data, true)));
