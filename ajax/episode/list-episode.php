<?php
if (!defined('NotSupportHacker')) die("You are illegally infiltrating our website");
header('Content-Type: application/json');
$movieId = ($_GET['movieId'] ? (int)$_GET['movieId'] : die());
if (get_total('movie', "WHERE id = $movieId") <= 0) die();
$total_episode = get_total('episode', "WHERE movieId = $movieId");
if ($total_episode <= 0) die();
$limit_page = $cf['limit_episode'];


$total_pages = ceil($total_episode / $limit_page);
$episode = '';
$dropmenu = '';
$current_page = '';
// Hiển thị các trang và phạm vi của mỗi trang
for ($page = 1; $page <= $total_pages; $page++) {
    $start_episode = ($page - 1) * $limit_page;
    $end_episode = min($start_episode + $limit_page - 1, $total_episode);
    $data_range = ($start_episode <= 0 ? 1 : $start_episode) . "-$end_episode";
    if ($start_episode < $end_episode) {
        if ($page <= 1) {
            $current_page = $data_range;
        }
        $dropmenu .= '<div class="dropdown-item ' . ($page <= 1 ? 'active' : '') . '" data-value="' . $data_range . '">' . $data_range . '</div>';
        $episode .= '<ul class="episodes ep-range" data-range="' . $data_range . '" style="' . ($page <= 0 ? '' : 'display: none;') . '">';
        $episode .= episode_list($movieId, $start_episode, $limit_page); // In ra các tập phim của trang hiện tại
        $episode .= '</ul>';
    }
}

$episode_html = '<div class="ep-filters">
    <div class="dropdown filter range">
        <button data-toggle="dropdown" class="dropdown-toggle btn btn-sm btn-primary" id="current-page">' . $current_page . '</button>
        <div class="dropdown-menu">
            ' . $dropmenu . '
        </div>
    </div>
    <div class="filter name">
        <input id="search-ep" class="form-control form-control-sm" placeholder="Episode number">
    </div>
</div>' . $episode;

die(json_encode([
    "html" => $episode_html,
    "status" => true,
    "totalItems" => $total_episode
]));
